using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace CreateOptimizedCGFAssay
{
    [Serializable]
    public class ComparePartitions
    {
        private readonly double _sid1;
        private readonly int _sid1Groups;
        private readonly double _sid1High;
        private readonly double _sid1Low;
        private int _a;
        private double _adjustedRand;
        private int _b;
        private int _c;
        private Dictionary<int, Dictionary<int, int>> _contTable = new Dictionary<int, Dictionary<int, int>>();

        private int _d;
        private List<int> _headers1 = new List<int>();
        private List<int> _headers2 = new List<int>();
        private int _n;
        private double _rand;
        private Dictionary<int, int> _sumCol = new Dictionary<int, int>();
        private Dictionary<int, int> _sumRow = new Dictionary<int, int>();
        private int _total;

        private double _wallace1Vs2;
        private double _wallace2Vs1;

        public ComparePartitions(int[] ar1, int[] ar2)
        {
            GetFullContTable(ar1, ar2);
            GetMisMatchMatrix();
            CalculateWallace();
            CalculateRand();
            CalculateAdjustedRand();
            CalculateSimpsonsIndexOfDiversity(ar1, out _sid1, out _sid1Low, out _sid1High, out _sid1Groups);
            //CalculateSimpsonsIndexOfDiversity(ar2, out _sid2, out _sid2Low, out _sid2High, out _sid2Groups);
        }

        public double Sid1
        {
            get { return _sid1; }
        }

        public double Sid1Low
        {
            get { return _sid1Low; }
        }

        public double Sid1High
        {
            get { return _sid1High; }
        }

        public int Sid1Groups
        {
            get { return _sid1Groups; }
        }


        public int A
        {
            get { return _a; }
        }

        public int B
        {
            get { return _b; }
        }

        public int C
        {
            get { return _c; }
        }

        public int D
        {
            get { return _d; }
        }

        public int N
        {
            get { return _n; }
        }

        public int Total
        {
            get { return _total; }
        }

        [Browsable(false)]
        public Dictionary<int, int> SumRow
        {
            get { return _sumRow; }
        }

        [Browsable(false)]
        public Dictionary<int, int> SumCol
        {
            get { return _sumCol; }
        }

        [Browsable(false)]
        public List<int> Headers2
        {
            get { return _headers2; }
        }

        [Browsable(false)]
        public List<int> Headers1
        {
            get { return _headers1; }
        }

        public double Wallace1Vs2
        {
            get { return _wallace1Vs2; }
        }

        public double Wallace2Vs1
        {
            get { return _wallace2Vs1; }
        }

        public double Rand
        {
            get { return _rand; }
        }

        public double AdjustedRand
        {
            get { return _adjustedRand; }
        }

        private void GetFullContTable(int[] ar1, int[] ar2)
        {
            _contTable = ContTable(ar1, ar2);
            GetContTableHeaders(_contTable, ref _headers1, ref _headers2);
            GetContTableTotals(_contTable, _headers1, _headers2, ref _sumCol, ref _sumRow, out _total);
        }

        /// <summary>Get the contigency table for frequency of different values for one array vs another array of int values.</summary>
        /// <param name="ar1">Array of ints</param>
        /// <param name="ar2">Array of ints</param>
        /// <returns>Contigency table of counts for frequency of different values.</returns>
        private static Dictionary<int, Dictionary<int, int>> ContTable(int[] ar1, int[] ar2)
        {
            var cont = new Dictionary<int, Dictionary<int, int>>();

            for (int i = 0; i < ar1.Length; i++)
            {
                int keyArray1 = ar1[i];
                int keyArray2 = ar2[i];
                if (cont.ContainsKey(keyArray1))
                {
                    if (cont[keyArray1].ContainsKey(keyArray2))
                    {
                        cont[keyArray1][keyArray2]++;
                    }
                    else
                    {
                        cont[keyArray1].Add(keyArray2, 1);
                    }
                }
                else
                {
                    cont.Add(keyArray1, new Dictionary<int, int> {{keyArray2, 1}});
                }
            }
            return cont;
        }

        private static void GetContTableHeaders(Dictionary<int, Dictionary<int, int>> cont, ref List<int> headers1,
                                                ref List<int> headers2)
        {
            var uniqueHeaders2 = new HashSet<int>();
            foreach (int key1 in cont.Keys)
            {
                headers1.Add(key1);
                foreach (int key2 in cont[key1].Keys)
                {
                    uniqueHeaders2.Add(key2);
                }
            }
            headers2.AddRange(uniqueHeaders2);
        }

        private static void GetContTableTotals(Dictionary<int, Dictionary<int, int>> cont, List<int> headers1,
                                               List<int> headers2, ref Dictionary<int, int> sumCol,
                                               ref Dictionary<int, int> sumRow, out int total)
        {
            foreach (int header2 in headers2)
            {
                sumRow.Add(header2, 0);
                foreach (int header1 in headers1)
                {
                    if (!cont[header1].ContainsKey(header2)) continue;
                    int val = cont[header1][header2];
                    sumRow[header2] += val;
                    if (sumCol.ContainsKey(header1))
                    {
                        sumCol[header1] += val;
                    }
                    else
                    {
                        sumCol.Add(header1, val);
                    }
                }
            }
            total = 0;
            foreach (int header1 in headers1)
            {
                total += sumCol[header1];
            }
        }

        private void GetMisMatchMatrix()
        {
            _a = 0;
            foreach (int header1 in _headers1)
            {
                foreach (int header2 in _headers2)
                {
                    if (!_contTable[header1].ContainsKey(header2)) continue;
                    int val = _contTable[header1][header2];
                    _a += (val*(val - 1))/2;
                }
            }
            int a1 = 0;
            foreach (int val in _sumCol.Values)
            {
                a1 += (val*(val - 1))/2;
            }
            _b = a1 - _a;

            int a2 = 0;
            foreach (int val in _sumRow.Values)
            {
                a2 += (val*(val - 1))/2;
            }
            _c = a2 - _a;

            _n = _total;
            _d = ((_n*(_n - 1))/2) - a1 - _c;
        }

        private void CalculateRand()
        {
            _rand = (_a + _d)/(double) (_a + _b + _c + _d);
        }

        private void CalculateAdjustedRand()
        {
            _adjustedRand = (((_n*(_n - 1))/2d)*(_a + _d) - ((_a + _b)*(_a + _c) + (_c + _d)*(_b + _d)))/
                            (Math.Pow((_n*(_n - 1))/2d, 2d) - ((_a + _b)*(_a + _c) + (_c + _d)*(_b + _d)));
        }

        private void CalculateWallace()
        {
            if (_a + _b > 0)
            {
                _wallace1Vs2 = _a/(double) (_a + _b);
            }
            else
            {
                _wallace1Vs2 = 0;
            }

            if (_a + _c > 0)
            {
                _wallace2Vs1 = _a/(double) (_a + _c);
            }
            else
            {
                _wallace2Vs1 = 0;
            }
        }


        public static void CalculateSimpsonsIndexOfDiversity(int[] ar, out double sid, out double sidLow,
                                                             out double sidHigh, out int sidGroups)
        {
            int n = ar.Length;
            var dict = new Dictionary<int, int>();
            foreach (int s in ar)
            {
                if (dict.ContainsKey(s))
                {
                    dict[s]++;
                }
                else
                {
                    dict.Add(s, 1);
                }
            }

            int sumTotal = 0;
            double sumFc2 = 0;
            double sumFc3 = 0;

            foreach (int i in dict.Values)
            {
                sumTotal += (i*(i - 1));
                sumFc2 += Math.Pow(i/(double) n, 2d);
                sumFc3 += Math.Pow(i/(double) n, 3d);
            }

            if ((n*(n - 1)) > 0)
            {
                sid = 1d - (sumTotal/(double) (n*(n - 1)));
            }
            else
            {
                sid = 1;
            }

            double sqSumFc2 = Math.Pow(sumFc2, 2d);
            double s2 = (4/(double) n)*(sumFc3 - sqSumFc2);

            sidLow = sid - 2*Math.Sqrt(s2);
            sidHigh = sid + 2*Math.Sqrt(s2);

            sidGroups = dict.Count;
        }
    }
}