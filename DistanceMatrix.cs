﻿using System;

namespace CreateOptimizedCGFAssay
{
    [Serializable]
    public class DistanceMatrix
    {
        private readonly int _numTaxa;
        private int _size;
        public int NumTaxa { get { return _numTaxa; } }
        public int Size { get { return _size; } }
        public string[] Taxa { get { return _taxa; } }
        public float[] Distances { get { return _distances; } }
        public float[] R { get { return _r; } }
        public float[] R2 { get { return _r2; } }
        private readonly string[] _taxa;

        private readonly float[] _distances;
        public int DIndex;
        private float[] _r;
        public int RIndex;
        private float[] _r2;
        public int R2Index;

        public DistanceMatrix(double[][] distMatrix, string[] taxa)
        {
            _taxa = taxa;
            _numTaxa = distMatrix.Length;
            _size = distMatrix.Length;
            DIndex = 0;
            _distances = new float[_numTaxa * (_numTaxa +1) / 2];

            for (int i = 0; i < _numTaxa; i++)
            {
                for (int j = 0; j < _numTaxa; j++)
                {
                    if (i < j)
                    {
                        int index = NJMap(i, j, _numTaxa);
                        _distances[index] = (float) distMatrix[i][j];
                    }
                }
            }

        }

        /// <summary>This function computes the r column in our matrix</summary>
        public void InitR()
        {
            _r = new float[_size];
            _r2 = new float[_size];
            RIndex = 0;
            R2Index = 0;
            int size1 = _size - 1;
            float size2 = _size - 2;

            int index = 0;
            for (int i = 0; i < size1; i++)
            {
                index++;
                for (int j = i+1; j < _size; j++)
                {
                    _r[i] += _distances[index];
                    
                    _r[j] += _distances[index];
                    index++;
                }
                _r2[i] = _r[i] / size2;
            }
            //PrintR();
        }

        /// <summary>
        /// Compute post-join changes to r-vector.  In this case, we decrement all of the accumulated distances 
        /// in the r-vector for the two nodes that were recently joined (i.e.  x, y)
        /// </summary>
        /// <param name="a">The index of one of the taxa that were joined</param>
        /// <param name="b">The index of the other taxa that was joined</param>
        /// Taken from ClearCut:
        ///  This vector of floats is used as a summary of overall distances from
        ///  each entry in the distance matrix to every other entry.  These values
        ///  are then used when computing the transformed distances from which
        ///  decisions concerning joining are made.
        ///  For speed, we don't recompute r from scratch.  Instead, we decrement
        ///  all entries in r by the appropriate amount.  That is, r[i] -= dist(i, a)
        ///  and r[i] -= dist(i, b).
        ///  As a speed optimization, I process the rows altogether for cache locality
        ///  purposes, and then process columns.
        ///  The processing of the scaled r matrix (r2) is handled on-the-fly elsewhere.
        public void ComputeR(int a, int b)
        {
            //  Loop through the rows and decrement the stored r values 
            //  by the distances stored in the rows and columns of the distance 
            //  matrix which are being removed post-join.
            // 
            //  We do the rows altogether in order to benefit from cache locality.

            int rI = RIndex + a + 1;
            int x = NJMap(a, a + 1, _size) + DIndex;
            int y = NJMap(b, b + 1, _size) + DIndex;
            for (int i = a+1; i < _size; i++)
            {
                _r[rI] -= _distances[x++];
                if (i > b)
                {
                    _r[rI] -= _distances[y++];
                }
                rI++;
            }

            //Similar to the above loop, we now do the columns
            rI = RIndex;
            x = NJMap(0, a, _size) + DIndex;
            y = NJMap(0, b, _size) + DIndex;
            for (int i = 0; i < b; i++)
            {
                if (i < a)
                {
                    _r[rI] -= _distances[x];
                    x += _size - i - 1;
                }
                _r[rI] -= _distances[y];
                y += _size - i - 1;
                rI++;
            }
            //PrintR();
        }


        /// <summary>Thus function maps i, j coordinates to the correct offset into the distance matrix</summary>
        /// <param name="i">1st dimension index.</param>
        /// <param name="j">2nd dimension index.</param>
        /// <param name="ntaxa">Number of taxa.</param>
        /// <returns></returns>
        public static int NJMap(int i, int j, int ntaxa)
        {
            return ((i * (2 * ntaxa - i - 1)) / 2 + j);
        }

        /// <summary>Find the smallest transformed value to identify which nodes to join.</summary>
        /// <param name="indexI">The row of the smallest transformed distance (by reference)</param>
        /// <param name="indexJ">The col of the smallest transformed distance (by reference)</param>
        /// <returns>The minimimum transformed distance</returns>
        /// Used only with traditional Neighbor-Joining, this function checks the entire
        /// working distance matrix and identifies the smallest transformed distance.
        /// This requires traversing the entire diagonal matrix, which is itself a 
        /// O(N^2) operation.
        public void FindMinTransformedDistance(out int indexI, out int indexJ)
        {
            double minDist = double.MaxValue;

            int dmIndex = DIndex;
            indexI = -1;
            indexJ = -1;

            for (int i = 0; i < _size; i++)
            {
                dmIndex++; //skip diagonal
                for (int j = i+1; j < _size; j++)
                {
                    double currentDist = _distances[dmIndex++] - (_r2[i + R2Index] + _r2[j + R2Index]);
                    if (currentDist < minDist)
                    {
                        minDist = currentDist;
                        indexI = i;
                        indexJ = j;
                    }
                }
            }
        }

        /// <summary>
        /// Collapse the distance matrix by removing 
        /// rows a and b from the distance matrix and
        /// replacing them with a single new row which 
        /// represents the internal node joining a and b
        /// </summary>
        /// <param name="a">An index to a row in the distance matrix from which we 
        /// joined.  This row will be collapsed.</param>
        /// <param name="b">An index to a row in the distance matrix from which we
        /// joined.  This row will be collapsed.</param>
        public void Collapse(int a, int b)
        {

            /* Taken from ClearCut:
            * DESCRIPTION:
            * ------------
            *
            * This function collapses the distance matrix in a way which optimizes
            * cache locality and ultimately gives us a speed improvement due to
            * cache.   At this point, we've decided to join rows a and b from
            * the distance matrix.  We will remove rows a and b from the distance  
            * matrix and replace them with a new row which represents the internal
            * node which joins rows a and b together. 
            * 
            * We always keep the matrix as compact as possible in order to 
            * get good performance from our cache in subsequent operations.  Cache
            * is the key to good performance here.  
            * 
            * Key Steps:
            * ----------
            * 
            *  1)  Fill the "a" row with the new distances of the internal node
            *      joining a and b to all other rows.  
            *  2)  Copy row 0 into what was row b
            *  3)  Increment the pointer to the start of the distance matrix
            *      by one row.
            *  4)  Decrement the size of the matrix by one row.
            *  5)  Do roughly the same thing to the r vector in order to
            *      keep it in sync with the distance matrix.
            *  6)  Compute the scaled r vector (r2) based on the updated
            *      r vector
            *
            * This keeps the distance matrix as compact as possible in memory, and
            * is a relatively fast operation. 
            *
            * This function requires that a < b
            *
            */

            int i;     /* index used for looping */
            float cval;     /* stores distance information during loop */

            

            /* We must assume that a < b */
            //               if(a >= b) {
            //                 fprintf(stderr, "Clearcut: (a<b) constraint check failed in NJ_collapse()\n");
            //                 exit(0);
            //               }


            /* compute the distance from the clade components (a, b) to the new node */
            float a2Clade = ((_distances[NJMap(a, b, _size) + DIndex]) + (_r2[a+ R2Index] - _r2[b + R2Index])) / 2.0f;
            float b2Clade = ((_distances[NJMap(a, b, _size) + DIndex]) + (_r2[b + R2Index] - _r2[a + R2Index])) / 2.0f;


            _r[a + RIndex] = 0.0f;  /* we are removing row a, so clear dist. in r */

            /* 
             * Fill the horizontal part of the "a" row and finish computing r and r2 
             * we handle the horizontal component first to maximize cache locality
             */
            float size3 = _size - 3;
            int indexA = NJMap(a, a + 1, _size) + DIndex;
            int indexB = NJMap(a + 1, b, _size) + DIndex;
            for (i = a + 1; i < _size; i++)
            {

                //Compute distance from new internal node to others in the distance matrix.
                cval = ((_distances[indexA] - a2Clade) + (_distances[indexB] - b2Clade)) / 2.0f;

                //incr. row b pointer differently depending on where i is in loop
                if (i < b)
                {
                    indexB += _size - i - 1;  /* traverse vertically  by incrementing by row */
                }
                else
                {
                    indexB++; //traverse horiz. by incrementing by column
                }

                //assign the newly computed distance and increment a ptr by a column
                _distances[indexA++] = cval;

                //accumulate the distance onto the r vector
                _r[a + RIndex] += cval;
                _r[i + RIndex] += cval;

                /* scale r2 on the fly here */
                _r2[i + R2Index] = _r[i + RIndex] / size3;
            }

            /* fill the vertical part of the "a" column and finish computing r and r2 */
            indexA = a + DIndex;  /* start at the top of the columb for "a" */
            indexB = b + DIndex;  /* start at the top of the columb for "b" */
            for (i = 0; i < a; i++)
            {
                /* 
                 * Compute distance from new internal node to others in 
                 * the distance matrix.
                 */
                cval = ((_distances[indexA] - a2Clade) + (_distances[indexB] - b2Clade)) / 2.0f;

                /* assign the newly computed distance and increment a ptr by a column */
                _distances[indexA] = cval;

                /* accumulate the distance onto the r vector */
                _r[a + RIndex] += cval;
                _r[i + RIndex] += cval;

                /* scale r2 on the fly here */
                _r2[i + R2Index] = _r[i + RIndex] / size3;

                /* here, always increment by an entire row */
                indexA += _size - i - 1;
                indexB += _size - i - 1;
            }

            //scale r2 on the fly here
            _r2[a + R2Index] = _r[a + RIndex] / size3;

            //Copy row 0 into row b.  Again, the code is structured into two loops 
            //to maximize cache locality for writes along the horizontal component of row b.
            int vptr = DIndex;
            indexB = b + DIndex;
            for (i = 0; i < b; i++)
            {
                _distances [indexB] = _distances[vptr++];
                indexB += _size - i - 1;
            }
            vptr++;  // skip over the diagonal
            indexB = NJMap(b, b + 1, _size) + DIndex;
            for (i = b + 1; i < _size; i++)
            {
                _distances [indexB++] = _distances [vptr++];
            }

            //Collapse r here by copying contents of r[0] into r[b] and incrementing 
            //pointer to the beginning of r by one row
            _r[b+ RIndex] = _r[0 + RIndex];
            RIndex++;


            //Collapse r2 here by copying contents of r2[0] into r2[b] and 
            //incrementing pointer to the beginning of r2 by one row
            _r2[b + R2Index] = _r2[0 + R2Index];
            R2Index++;

            //increment dmat pointer to next row
            DIndex += _size;

            //decrement the total size of the distance matrix by one row
            _size--;
        }

    }
}
