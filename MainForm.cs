﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace CreateOptimizedCGFAssay
{
    public partial class MainForm : Form
    {
        private readonly List<MarkerSetAnalysis> _analyses = new List<MarkerSetAnalysis>();
        private readonly Dictionary<string, int> _refClustersLow = new Dictionary<string, int>();
        private readonly Dictionary<string,int> _refClustersHigh = new Dictionary<string, int>();
        private readonly List<SymDCount> _symDCounts = new List<SymDCount>();
        private readonly List<WallaceCount> _wallaceCounts = new List<WallaceCount>();


        /// <summary>Binarized BLAST report file path.</summary>
        private FileInfo _binarizedDataFile;

        private double _clustersAt = 95d;

        private long _iterations = 1000;
        private int _markerCount = 20;
        private double _refClustersAt = 95d;
        private string _refTree = "";
        private int _setsToKeep = 10;
        

        public MainForm()
        {
            InitializeComponent();

            openToolStripMenuItem.Click += (sender, args) => Open();
            btnRunAnalysis.Click += (sender, args) => RunAnalysis();
            btnRun.Click += (sender, args) => RunAnalysis();

            //allow file drop to open binarized data file
            DragOver += (sender, args) => OnDragOverOrEnter(args);
            DragEnter += (sender, args) => OnDragOverOrEnter(args);
            DragDrop += (sender, args) => OnFileDrop(args);

            txtIterations.KeyPress += (sender, args) => OnTextboxKeyPress(args);
            txtMarkers.KeyPress += (sender, args) => OnTextboxKeyPress(args);
            txtSetsToKeep.KeyPress += (sender, args) => OnTextboxKeyPress(args);

            txtIterations.LostFocus += (sender, args) => SetAnalysisVariables();
            txtMarkers.LostFocus += (sender, args) => SetAnalysisVariables();
            txtSetsToKeep.LostFocus += (sender, args) => SetAnalysisVariables();

            saveAllToolStripMenuItem.Click += (sender, args) => SaveAllAnalyses();

            //allow drag drop into form on form load
            Load += (sender, args) => AllowDrop = true;


            txtClustersAt.KeyPress += (sender, e) => OnTxtClustersAtOnKeyPress(e);
            txtClustersAt.LostFocus += (sender, args) => OnTxtClustersAtOnKeyPress(new KeyPressEventArgs((char)Keys.Enter));
            txtRefClustersAt.KeyPress += (sender, e) => GetRefClustersAt(e);
            txtRefClustersAt.LostFocus += (sender, args) => GetRefClustersAt(new KeyPressEventArgs((char)Keys.Enter));

            txtRefClustersLow.TextChanged += (sender, args) => GetRefClustersLow();
            txtRefClustersHigh.TextChanged += (sender, args) => GetRefClustersHigh();
            txtRefTree.TextChanged += (sender, args) => ValidateRefTreeFormat();

            bgw.DoWork += (sender, args) => OnRunAnalysisUsingBgw();
            bgw.ProgressChanged += (sender, args) => OnAnalysisProgressChanged(args);
            bgw.RunWorkerCompleted += (sender, args) => OnAnalysisCompleted();

            txtRefTree.AllowDrop = true;
            txtRefTree.DragEnter += (sender, args) => OnDragOverOrEnter(args);
            txtRefTree.DragOver += (sender, args) => OnDragOverOrEnter(args);
            txtRefTree.DragDrop += (sender, args) => ReadIntoTextBox(args, txtRefTree);

            txtRefClustersLow.AllowDrop = true;
            txtRefClustersLow.DragEnter += (sender, args) => OnDragOverOrEnter(args);
            txtRefClustersLow.DragOver += (sender, args) => OnDragOverOrEnter(args);
            txtRefClustersLow.DragDrop += (sender, args) => ReadIntoTextBox(args, txtRefClustersLow);

            txtRefClustersHigh.AllowDrop = true;
            txtRefClustersHigh.DragEnter += (sender, args) => OnDragOverOrEnter(args);
            txtRefClustersHigh.DragOver += (sender, args) => OnDragOverOrEnter(args);
            txtRefClustersHigh.DragDrop += (sender, args) => ReadIntoTextBox(args, txtRefClustersHigh);

        }

        private static void OnDragOverOrEnter(DragEventArgs args)
        {
            if (args.Data.GetDataPresent(DataFormats.FileDrop)) args.Effect = DragDropEffects.Copy;
        }

        private static void ReadIntoTextBox(DragEventArgs args, RichTextBox textBox)
        {
            if (!args.Data.GetDataPresent(DataFormats.FileDrop)) return;
            var filepaths = (string[])args.Data.GetData(DataFormats.FileDrop);
            if (filepaths.Length == 1)
            {
                textBox.Text = new StreamReader(filepaths[0]).ReadToEnd();
            }
        }

        private void ValidateRefTreeFormat()
        {
            try
            {
                string text = txtRefTree.Text;

                text = text.Replace("\r", "").Replace("\n", "");

                var regex = new Regex(@"[(,](\w+)[:]", RegexOptions.Compiled);

                MatchCollection matches = regex.Matches(text);
                int count = 0;
                foreach (Match match in matches)
                {
                    text = text.Replace(match.Groups[1].Value, count.ToString(CultureInfo.InvariantCulture));
                    count++;
                }
                NewickParser.Parse(text);
                _refTree = txtRefTree.Text;
                txtRefClustersAt.Enabled = txtRefClustersLow.Text == "";
            }
            catch (Exception)
            {
                MessageBox.Show("Newick tree file format expected.");
                txtRefTree.Text = "";
                _refTree = "";
            }
        }

        private void GetRefClustersAt(KeyPressEventArgs e)
        {
            int i;
            if (e.KeyChar == '\b' || e.KeyChar == '.' || int.TryParse(e.KeyChar.ToString(CultureInfo.InvariantCulture), out i))
            {
                e.Handled = false;
            }
            else if (e.KeyChar == (char)Keys.Enter || e.KeyChar == '\t')
            {
                string text = txtRefClustersAt.Text;
                double d;
                if (double.TryParse(text, out d))
                {
                    _refClustersAt = d;
                }
            }
            else
            {
                e.Handled = true;
            }
        }

        private void GetRefClustersLow()
        {
            string text = txtRefClustersLow.Text;
            _refClustersLow.Clear();
            using (var sr = new StringReader(text))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    string[] split = line.Split('\t');
                    if (split.Length < 2)
                        continue;

                    int cluster;
                    if (!(int.TryParse(split[1], out cluster)))
                        continue;
                    _refClustersLow.Add(split[0], cluster);
                }
            }
            txtRefClustersAt.Enabled = txtRefTree.Text == "";
        }


        private void GetRefClustersHigh()
        {
            string text = txtRefClustersHigh.Text;
            _refClustersHigh.Clear();
            using (var sr = new StringReader(text))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    string[] split = line.Split('\t');
                    if (split.Length < 2)
                        continue;

                    int cluster;
                    if (!(int.TryParse(split[1], out cluster)))
                        continue;
                    _refClustersHigh.Add(split[0], cluster);
                }
            }
            txtRefClustersAt.Enabled = txtRefTree.Text == "";
        }

        private void OnTxtClustersAtOnKeyPress(KeyPressEventArgs e)
        {
            int i;
            if (e.KeyChar == '\b' || e.KeyChar == '.' || int.TryParse(e.KeyChar.ToString(CultureInfo.InvariantCulture), out i))
            {
                e.Handled = false;
            }
            else if (e.KeyChar == (char)Keys.Enter || e.KeyChar == '\t')
            {
                string text = txtClustersAt.Text;

                double d;
                if (double.TryParse(text, out d))
                {
                    _clustersAt = d;
                }
            }
            else
            {
                e.Handled = true;
            }
        }

        private void SaveAllAnalyses()
        {
            foreach (MarkerSetAnalysis analysis in _analyses)
            {
                analysis.SaveToBinFile(Path.Combine(Environment.CurrentDirectory,
                                                    string.Format(
                                                                  "CGF_Marker_Analysis-{0}_markers-{1}_iterations-{2}_.bin",
                                                                  analysis.MarkerCount,
                                                                  analysis.Iterations,
                                                                  DateTime.Now.ToString("MM_dd_yyyy"))));
            }
        }

        private void OnTextboxKeyPress(KeyPressEventArgs e)
        {
            int i;
            if (int.TryParse(e.KeyChar.ToString(CultureInfo.InvariantCulture), out i) || e.KeyChar == '\b')
            {
                e.Handled = false;
            }
            else if (e.KeyChar == (char)Keys.Enter || e.KeyChar == (char)Keys.Tab)
            {
                SetAnalysisVariables();
            }
            else
            {
                e.Handled = true;
            }
        }

        private void SetAnalysisVariables()
        {
            int i;
            if (int.TryParse(txtMarkers.Text, out i))
            {
                _markerCount = i;
            }
            else
            {
                txtMarkers.Text = _markerCount.ToString(CultureInfo.InvariantCulture);
                MessageBox.Show(string.Format("Incorrect input for number of markers. Reset to {0}.", _markerCount));
            }
            if (int.TryParse(txtSetsToKeep.Text, out i))
            {
                _setsToKeep = i;
            }
            else
            {
                txtSetsToKeep.Text = _setsToKeep.ToString(CultureInfo.InvariantCulture);
                MessageBox.Show(string.Format("Incorrect input for number of marker sets to keep. Reset to {0}.",
                                              _setsToKeep));
            }

            long l;
            if (long.TryParse(txtIterations.Text, out l))
            {
                _iterations = l;
            }
            else
            {
                txtIterations.Text = _iterations.ToString(CultureInfo.InvariantCulture);
                MessageBox.Show(string.Format("Incorrect input for number of iterations. Reset to {0}.", _iterations));
            }
        }

        /// <summary>When the user drops a file into the form, set it as the binarized data file.</summary>
        /// <param name="e">DataFormats.FileDrop data is expected.</param>
        private void OnFileDrop(DragEventArgs e)
        {
            if (!e.Data.GetDataPresent(DataFormats.FileDrop))
                return; //proceed on file drop data
            var files = (string[])e.Data.GetData(DataFormats.FileDrop);
            if (files.Length == 1)
            {
                var fileInfo = new FileInfo(files[0]);
                //binary file will open a previous analysis
                string ext = fileInfo.Extension;
                if (ext == ".bin")
                {
                    Console.WriteLine("File is a binary file.");
                    LoadAnalysisFromBinary(fileInfo.FullName);
                }
                else
                {
                    Console.WriteLine("File is not a binary file.");
                    //set the single file drop as the binarized data file
                    _binarizedDataFile = fileInfo;
                    lblStatus.Text = string.Format("File loaded: {0}", _binarizedDataFile.FullName);
                    btnRun.Enabled = true;
                    btnRunAnalysis.Enabled = true;
                }
            }
            else
            {
                foreach (string file in files)
                {
                    Console.WriteLine(file);
                    var fileInfo = new FileInfo(file);
                    if (fileInfo.Extension == ".bin")
                    {
                        LoadAnalysisFromBinary(fileInfo.FullName);
                    }
                }
            }
        }

        private void LoadAnalysisFromBinary(string filename)
        {
            Console.WriteLine("Trying to load analysis from binary file.");
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);
            var analysis = (MarkerSetAnalysis)formatter.Deserialize(stream);
            stream.Close();
            Console.WriteLine("Analysis loaded from binary file.");
            new ResultsForm(analysis).Show();
            _analyses.Add(analysis);
        }

        /// <summary>When the user clicks on the Open button, prompt the user with an open file dialog box for selection of a binarized data file.</summary>
        private void Open()
        {
            using (var ofd = new OpenFileDialog())
            {
                ofd.Multiselect = false;
                ofd.Title = "Select the binarized BLAST report file.";

                if (ofd.ShowDialog() != DialogResult.OK)
                    return;
                _binarizedDataFile = new FileInfo(ofd.FileName);
                lblStatus.Text = string.Format("File Loaded: {0}", _binarizedDataFile.FullName);
                btnRun.Enabled = true;
                btnRunAnalysis.Enabled = true;
            }
        }

        private void RunAnalysis()
        {
            EnableControls(false);
            progressbar.Visible = true;
            progressbar.Value = 0;
            bgw.RunWorkerAsync();
        }

        private void OnRunAnalysisUsingBgw()
        {
            MarkerSetAnalysis analysis;
            if (_refClustersLow.Count > 0)
            {
                if (_refTree == "")
                    analysis = new MarkerSetAnalysis(
                        _binarizedDataFile,
                        _setsToKeep,
                        _iterations,
                        _markerCount,
                        _clustersAt,
                        _refClustersLow,
                        _refClustersHigh);
                else
                {
                    analysis = new MarkerSetAnalysis(
                        _binarizedDataFile,
                        _setsToKeep,
                        _iterations,
                        _markerCount,
                        _clustersAt,
                        _refClustersLow,
                        _refClustersHigh,
                        _refTree);
                }
            }
            else
            {
                analysis = new MarkerSetAnalysis(_binarizedDataFile,
                                                 _setsToKeep,
                                                 _iterations,
                                                 _markerCount,
                                                 _clustersAt,
                                                 _refClustersAt);
            }
            analysis.CreateMarkerSets(bgw);
            _analyses.Add(analysis);
            analysis.SaveToBinFile(Path.Combine(Environment.CurrentDirectory,
                                                string.Format(
                                                              "CGF_Marker_Analysis-{0}_markers-{1}_iterations-{2}_.bin",
                                                              analysis.MarkerCount,
                                                              analysis.Iterations,
                                                              DateTime.Now.ToString("MM_dd_yyyy"))));
        }

        private void OnAnalysisProgressChanged(ProgressChangedEventArgs e)
        {
            progressbar.Value = e.ProgressPercentage;

            var reporter = e.UserState as MarkerSetAnalysisReporter;
            if (reporter == null) return;
            string status = reporter.Status;

            //show how much time has elapsed, how much time is left, how many iterations have been done, whether new marker sets have been added
            status = string.Format("{0} - {1}", status,
                                   (reporter.NewMarkerSetAdded)
                                       ? "New marker set(s) added!"
                                       : "No new marker set(s) added");
            //update the status bar label to show the new status
            lblStatus.Text = status;

            string minMaxSymD = string.Format("Min SymD: {0}; Max SymD: {1}", reporter.MinSymD, reporter.MaxSymD);

            Dictionary<int, long> dict = reporter.SymDCounts;

            UpdateSymDGraph(minMaxSymD, dict);

            string minMaxWallace = string.Format("Min Wallace: {0:0.0000}; Max Wallace: {1:0.0000}", reporter.MinWallace, reporter.MaxWallace);

            Dictionary<double, long> wallaceDict = reporter.WallaceCounts;

            UpdateWallaceHistogram(minMaxWallace, wallaceDict);
        }

        private void UpdateSymDGraph(string minMaxSymD, Dictionary<int, long> dict)
        {
            _symDCounts.Clear();

            var keylist = new List<int>();
            foreach (int key in dict.Keys)
            {
                keylist.Add(key);
            }

            keylist.Sort();
            foreach (int i in keylist)
            {
                long count = dict[i];
                _symDCounts.Add(new SymDCount(i, count));
            }
            chartSymD.Legends.Clear();

            chartSymD.DataSource = _symDCounts;
            chartSymD.Titles[2].Text = minMaxSymD;
            chartSymD.Series[0].XValueMember = "SymD";
            Axis xaxis = chartSymD.ChartAreas[0].AxisX;
            xaxis.Interval = 10;
            xaxis.MajorGrid.Enabled = false;
            xaxis.MinorTickMark.Enabled = true;
            xaxis.MinorTickMark.Interval = 2;
            Axis yaxis = chartSymD.ChartAreas[0].AxisY;
            yaxis.MajorGrid.Enabled = false;
            chartSymD.Series[0].YValueMembers = "Count";
            chartSymD.DataBind();
        }

        private void UpdateWallaceHistogram(string minMaxWallace, Dictionary<double, long> wallaceDict)
        {
            _wallaceCounts.Clear();

            var wallaceBins = new List<double>();
            foreach (double key in wallaceDict.Keys)
            {
                wallaceBins.Add(key);
            }

            wallaceBins.Sort();
            foreach (double i in wallaceBins)
            {
                long count = wallaceDict[i];
                _wallaceCounts.Add(new WallaceCount(i, count));
            }

            chartWallace.Legends.Clear();
            chartWallace.Titles[2].Text = minMaxWallace;
            chartWallace.DataSource = _wallaceCounts;
            chartWallace.Series[0].XValueMember = "Wallace";
            Axis xaxisWallace = chartWallace.ChartAreas[0].AxisX;
            xaxisWallace.Minimum = 0;
            xaxisWallace.Maximum = 1;
            xaxisWallace.Interval = 0.1;
            xaxisWallace.MajorGrid.Enabled = false;
            xaxisWallace.MinorTickMark.Enabled = true;
            xaxisWallace.MinorTickMark.Interval = 0.025;
            Axis yaxisWallace = chartWallace.ChartAreas[0].AxisY;
            yaxisWallace.MajorGrid.Enabled = false;
            chartWallace.Series[0].YValueMembers = "Count";
            chartWallace.DataBind();
        }

        private void OnAnalysisCompleted()
        {
            progressbar.Visible = false;
            EnableControls(true);
            //show results in a new window
            new ResultsForm(_analyses[_analyses.Count - 1]).Show();
        }

        private void EnableControls(bool enable)
        {
            btnRunAnalysis.Enabled =
                openToolStripMenuItem.Enabled =
                txtIterations.Enabled =
                txtMarkers.Enabled =
                txtSetsToKeep.Enabled =
                btnRun.Enabled =
                enable;
        }

        private void ShowAllResults(object sender, EventArgs e)
        {
            foreach (MarkerSetAnalysis markerSetAnalysis in _analyses)
            {
                new ResultsForm(markerSetAnalysis).Show();
            }
        }
    }
}