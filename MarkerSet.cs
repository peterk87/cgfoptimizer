﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace CreateOptimizedCGFAssay
{
    [Serializable]
    public class MarkerSet
    {
        /// <summary>Binary absence/presence values list.</summary>
        private readonly List<List<bool>> _bin = new List<List<bool>>();

        [NonSerialized] private readonly FileInfo _binaryDataFile;
        private readonly List<string> _fingerprints = new List<string>();

        private readonly List<string> _genes;
        private readonly int _markerCount;
        [NonSerialized] private readonly Random _random;

        private readonly List<List<bool>> _refBinaryData;
        private readonly MarkerSet _referenceMarkerSet;
        private readonly int _sampleCount;
        private readonly List<string> _samples;
        private readonly TreeSplits _splits;
        private readonly Dictionary<string, List<string>> _uniqueFingerprints = new Dictionary<string, List<string>>();
        [NonSerialized] private string _binaryData;
        [NonSerialized] private string _binaryFasta;
        [NonSerialized] private string _clusteredSamples;
        private int[] _clustersLow;
        private ComparePartitions _comparePartitionsLow;
        [NonSerialized] private string _distMatrix;
        [NonSerialized] private double[][] _distanceMatrix;
        private List<int> _markers;
        private int _symmetricDifference;
        private NJTree _tree;
        private int[] _clustersHigh;
        private ComparePartitions _comparePartitionsHigh;

        /// <summary>Reference marker set constructor with user specified clustering threshold for reference set.</summary>
        /// <param name="binaryDataFile">User-specified binary data file.</param>
        /// <param name="refClustersAt">Reference clusters generated at specified threshold.</param>
        public MarkerSet(FileInfo binaryDataFile, double refClustersAt)
        {
            _binaryDataFile = binaryDataFile;
            _referenceMarkerSet = this;
            _samples = new List<string>();
            _genes = new List<string>();
            _refBinaryData = new List<List<bool>>();
            _random = new Random();

            //Read the binary data file
            Read();
            _markerCount = _uniqueFingerprints.Count;
            _sampleCount = _samples.Count;

            _distanceMatrix = CalculateMatrix(_refBinaryData, _sampleCount, _markerCount);

            GetNJTree();
            _splits = new TreeSplits(_tree, _sampleCount);
            //get clusters at user specified %
            GetClusters((100d - refClustersAt) / 100d);
        }

        /// <summary>Reference marker set constructor with user-specified referernce cluster numbers.</summary>
        /// <param name="binaryDataFile">User-specified binary data file.</param>
        /// <param name="refClustersLow">Reference clusters.</param>
        public MarkerSet(FileInfo binaryDataFile, Dictionary<string, int> refClustersLow, Dictionary<string, int> refClustersHigh )
        {
            _binaryDataFile = binaryDataFile;
            _referenceMarkerSet = this;
            _samples = new List<string>();
            _genes = new List<string>();
            _refBinaryData = new List<List<bool>>();
            _random = new Random();

            //Read the binary data file
            Read();
            _markerCount = _uniqueFingerprints.Count;
            _sampleCount = _samples.Count;

            _distanceMatrix = CalculateMatrix(_refBinaryData, _sampleCount, _markerCount);

            GetNJTree();
            _splits = new TreeSplits(_tree, _sampleCount);
            SetReferenceClusters(refClustersLow, refClustersHigh);
        }

        /// <summary>Reference marker set constructor with user-specified referernce cluster numbers.</summary>
        /// <param name="binaryDataFile">User-specified binary data file.</param>
        /// <param name="refClustersLow">Low resolution reference clusters.</param>
        /// <param name="refClustersHigh">High resolution reference clusters.</param>
        /// <param name="refTree">Reference newick format tree to be parsed into NJTree object.</param>
        public MarkerSet(FileInfo binaryDataFile, Dictionary<string, int> refClustersLow, Dictionary<string,int> refClustersHigh, string refTree)
        {
            _binaryDataFile = binaryDataFile;
            _referenceMarkerSet = this;
            _samples = new List<string>();
            _genes = new List<string>();
            _refBinaryData = new List<List<bool>>();
            _random = new Random();

            //Read the binary data file
            Read();
            _markerCount = _uniqueFingerprints.Count;
            _sampleCount = _samples.Count;

            _distanceMatrix = CalculateMatrix(_refBinaryData, _sampleCount, _markerCount);

            SetRefTreeFromNewick(refTree);
            _splits = new TreeSplits(_tree, _sampleCount);
            SetReferenceClusters(refClustersLow, refClustersHigh);
        }

        /// <summary>Create a random set of markerCount from a list of gene numbers.</summary>
        /// <param name="referenceMarkerSet">Reference marker set with all of the possible markers.</param>
        /// <param name="seed">Random number generator seed.</param>
        /// <param name="markerCount">Number of genes in random set.</param>
        public MarkerSet(MarkerSet referenceMarkerSet, int seed, int markerCount, double clustersAt)
        {
            _random = new Random(seed);

            _referenceMarkerSet = referenceMarkerSet;
            _binaryDataFile = _referenceMarkerSet.BinaryDataFile;
            _genes = _referenceMarkerSet.Genes;
            _uniqueFingerprints = _referenceMarkerSet.UniqueFingerprints;
            _fingerprints = _referenceMarkerSet.Fingerprints;
            _samples = _referenceMarkerSet.Samples;
            _refBinaryData = _referenceMarkerSet.RefBinaryData;
            _markerCount = markerCount;
            _sampleCount = _referenceMarkerSet.SampleCount;
            MakeRandSet();
            _distanceMatrix = CalculateMatrix(_bin, _sampleCount, _markerCount);
            GetNJTree();
            _splits = new TreeSplits(_tree, _sampleCount);
            //get clusters at threshold specified by the user (default: 95%)
            GetClusters((100d - clustersAt) / 100d);
            //calc SID, Rand, Adjusted Rand, Wallace coefficients
            GetPartitionAgreementCoefficients();
            _distanceMatrix = null;
        }

        public List<string> Fingerprints { get { return _fingerprints; } }

        public Dictionary<string, List<string>> UniqueFingerprints { get { return _uniqueFingerprints; } }

        public List<string> Genes { get { return _genes; } }

        public List<List<bool>> RefBinaryData { get { return _refBinaryData; } }

        public int MarkerCount { get { return _markerCount; } }

        public int SampleCount { get { return _sampleCount; } }

        public List<string> Samples { get { return _samples; } }

        public List<int> Markers { get { return _markers; } }

        public string BinaryData { get { return _binaryData ?? GetBinaryData(); } }

        public string BinaryFasta { get { return _binaryFasta ?? GetBinaryFasta(); } }

        public double[][] DistanceMatrix { get { return _distanceMatrix ?? CalculateMatrix(_bin, _sampleCount, _markerCount); } }

        public string DistMatrix { get { return _distMatrix ?? GetDistMatrix(); } }

        public MarkerSet ReferenceMarkerSet { get { return _referenceMarkerSet; } }

        public int SplitCount { get { return _splits.SplitCount; } }

        public string SplitIDs { get { return string.Join("\n", _splits.SplitIDs); } }

        public string Tree { get { return _tree.Write(); } }

        public int SymmetricDifference { get { return _symmetricDifference; } }

        public FileInfo BinaryDataFile { get { return _binaryDataFile; } }

        public string ClusteredSamples { get { return _clusteredSamples ?? GetClusteredSamples(); } }

        public double SimpsonsIndexOfDiversity
        {
            get
            {
                //for calculating SID for ref set
                double sid, sidlow, sidhigh;
                int sidgroups;
                ComparePartitions.CalculateSimpsonsIndexOfDiversity(_clustersLow,
                                                                    out sid,
                                                                    out sidlow,
                                                                    out sidhigh,
                                                                    out sidgroups);
                return sid;
            }
        }

        public double RandLow { get { return _comparePartitionsLow != null ? _comparePartitionsLow.Rand : 1; } }

        public double AdjustedRandLow { get { return _comparePartitionsLow != null ? _comparePartitionsLow.AdjustedRand : 1; } }

        public double WallaceRefVsSetLow { get { return _comparePartitionsLow != null ? _comparePartitionsLow.Wallace1Vs2 : 1; } }

        public double WallaceSetVsRefLow { get { return _comparePartitionsLow != null ? _comparePartitionsLow.Wallace2Vs1 : 1; } }


        public double RandHigh { get { return _comparePartitionsHigh != null ? _comparePartitionsHigh.Rand : 1; } }

        public double AdjustedRandHigh { get { return _comparePartitionsHigh != null ? _comparePartitionsHigh.AdjustedRand : 1; } }

        public double WallaceRefVsSetHigh { get { return _comparePartitionsHigh != null ? _comparePartitionsHigh.Wallace1Vs2 : 1; } }

        public double WallaceSetVsRefHigh { get { return _comparePartitionsHigh != null ? _comparePartitionsHigh.Wallace2Vs1 : 1; } }

        public int[] ClustersLow { get { return _clustersLow; } }
        public int[] ClustersHigh { get { return _clustersHigh; } }

        private void SetReferenceClusters(Dictionary<string, int> refClustersLow, Dictionary<string,int> refClustersHigh)
        {
            var list = new List<int>(_samples.Count);
            var listHigh = new List<int>(_samples.Count);
            foreach (string sample in _samples)
            {
                list.Add(refClustersLow[sample]);
                listHigh.Add(refClustersHigh[sample]);
            }
            _clustersLow = list.ToArray();
            _clustersHigh = listHigh.ToArray();
        }

        private string GetBinaryData()
        {
            if (_markers == null) return null;

            var list = new List<string>();

            var header = new List<string> {"Samples"};
            for (int i = 0; i < _markers.Count; i++)
            {
                string marker = _markers[i].ToString(CultureInfo.InvariantCulture);
                header.Add(marker);
            }
            list.Add(string.Join(",", header));

            for (int i = 0; i < _bin.Count; i++)
            {
                List<bool> bools = _bin[i];
                string sample = _samples[i];
                var row = new List<string> {sample};
                for (int j = 0; j < bools.Count; j++)
                {
                    bool b = bools[j];
                    row.Add(b ? "1" : "0");
                }
                list.Add(string.Join(",", row));
            }
            _binaryData = string.Join("\n", list);
            return _binaryData;
        }

        private string GetBinaryFasta()
        {
            if (_markers == null) return null;
            var list = new List<string>();
            for (int i = 0; i < _bin.Count; i++)
            {
                List<bool> bools = _bin[i];
                string sample = _samples[i];
                list.Add(string.Format(">{0}", sample));
                var list2 = new List<string>();
                for (int j = 0; j < bools.Count; j++)
                {
                    bool b = bools[j];
                    list2.Add(b ? "1" : "0");
                }
                list.Add(string.Join("", list2));
            }
            _binaryFasta = string.Join("\n", list);
            return _binaryFasta;
        }

        private string GetDistMatrix()
        {
            _distMatrix = WriteDistanceMatrixToStdIn();
            return _distMatrix;
        }

        private string GetClusteredSamples()
        {
            List<int> indices = _tree.GetTaxaIndexList();

            var list = new List<string>();
            foreach (int index in indices)
            {
                list.Add(_samples[index]);
            }

            _clusteredSamples = string.Join(",", list);
            return _clusteredSamples;
        }

        public List<string> GetSamplesInNJTreeOrder()
        {
            List<int> indices = _tree.GetTaxaIndexList();

            var list = new List<string>();
            foreach (int index in indices)
            {
                list.Add(_samples[index]);
            }
            return list;
        }

        private void GetClusters(double diff)
        {
            List<int> indices = _tree.GetTaxaIndexList();

            int clusterCount = 1;

            var strainIndicesClusters = new Dictionary<int, int> {{indices[0], clusterCount}};
            if (_distanceMatrix == null)
                _distanceMatrix = CalculateMatrix(_bin, _sampleCount, _markerCount);
            for (int i = 1; i < indices.Count; i++)
            {
                int index1 = indices[i - 1];
                int index2 = indices[i];

                double d = _distanceMatrix[index1][index2];
                if (d > diff)
                {
                    clusterCount++;
                }
                strainIndicesClusters.Add(indices[i], clusterCount);
            }

            //check if the first cluster group and last cluster group according to this simple algorithm are truly 
            //different clusters or if they actually share the same cluster number
            double firstLastGroupDifference = _distanceMatrix[indices[0]][indices[indices.Count - 1]];
            if (firstLastGroupDifference <= diff)
            {
                strainIndicesClusters[indices.Count - 1] = 1;
                for (int i = indices.Count - 1; i > 0; i--)
                {
                    int index1 = indices[i - 1];
                    int index2 = indices[i];

                    double d = _distanceMatrix[index1][index2];
                    if (d <= diff)
                    {
                        strainIndicesClusters[index1] = 1;
                    }
                    else
                    {
                        break;
                    }
                }
            }

            //reorder strains based on reference ordering
            var clusters = new List<int>();
            for (int i = 0; i < indices.Count; i++)
            {
                clusters.Add(strainIndicesClusters[i]);
            }
            _distanceMatrix = null;
            _clustersLow = clusters.ToArray();
        }

        private void GetPartitionAgreementCoefficients()
        {
            _comparePartitionsLow = new ComparePartitions(_referenceMarkerSet._clustersLow, _clustersLow);
            _comparePartitionsHigh = new ComparePartitions(_referenceMarkerSet._clustersHigh, _clustersLow);
        }

        /// <summary>Read the tab-delimited binarized BLAST report file with sample names in the first row. Marker name in the first column of each subsequent row.</summary>
        private void Read()
        {
            using (var sr = new StreamReader(_binaryDataFile.FullName))
            {
                string line = sr.ReadLine();
                if (string.IsNullOrEmpty(line))
                    throw new Exception(
                        "Binarized data file format not supported. Expected strain names in first row of file.");
                string[] split = line.Split('\t');
                if (split.Length == 1)
                    throw new Exception(
                        "Binarized data file format not supported. Expected tab-delimited strain names.");
                for (int i = 1; i < split.Length; i++)
                {
                    _samples.Add(split[i]);
                }
                var sb = new StringBuilder();
                var binary = new List<List<bool>>();
                while (!sr.EndOfStream)
                {
                    line = sr.ReadLine();

                    if (string.IsNullOrEmpty(line)) continue;

                    split = line.Split('\t');

                    string gene = split[0];
                    _genes.Add(gene);
                    var list = new List<bool>();
                    sb.Clear();
                    for (int i = 1; i < split.Length; i++)
                    {
                        string s = split[i];
                        list.Add(s == "1");
                        sb.Append(s);
                    }

                    string fingerprint = sb.ToString();

                    if (_uniqueFingerprints.ContainsKey(fingerprint))
                    {
                        _uniqueFingerprints[fingerprint].Add(gene);
                    }
                    else
                    {
                        _uniqueFingerprints.Add(fingerprint, new List<string> {gene});
                        _fingerprints.Add(fingerprint);
                        binary.Add(list);
                    }
                }


                //transpose the binary data 2D list to be samples by markers
                for (int i = 0; i < _samples.Count; i++)
                {
                    var list = new List<bool>();
                    for (int j = 0; j < _uniqueFingerprints.Count; j++)
                    {
                        bool b = binary[j][i];
                        list.Add(b);
                    }
                    _refBinaryData.Add(list);
                }
            }
        }

        public static void CalculateSymD(MarkerSet set)
        {
            set.GetSymD();
        }

        private void SetRefTreeFromNewick(string refTree)
        {
            refTree = refTree.Replace("\r", "").Replace("\n", "");
            int count = 0;
            foreach (string sample in _samples)
            {
                refTree = refTree.Replace(sample, count.ToString(CultureInfo.InvariantCulture));
                count++;
            }
            _tree = NewickParser.Parse(refTree);
        }

        private void GetNJTree()
        {
            var taxa = new string[_sampleCount];
            for (int i = 0; i < _sampleCount; i++)
            {
                taxa[i] = i.ToString(CultureInfo.InvariantCulture);
            }
            if (_distanceMatrix == null)
                _distanceMatrix = CalculateMatrix(_bin, _sampleCount, _markerCount);
            var matrix = new DistanceMatrix(_distanceMatrix, taxa);
            _tree = NJ.NeighborJoining(matrix);
        }

        private void GetSymD()
        {
            _symmetricDifference = TreeSplits.GetDifferences(_referenceMarkerSet._splits.SplitIDs, _splits.SplitIDs);
        }

        private string WriteDistanceMatrixToStdIn()
        {
            if (_distanceMatrix == null)
                _distanceMatrix = CalculateMatrix(_bin, _sampleCount, _markerCount);
            using (var sw = new StringWriter())
            {
                sw.WriteLine(_sampleCount);
                for (int i = 0; i < _sampleCount; i++)
                {
                    //string list for line contents
                    var list = new List<string>();
                    //sample name will be the index number of the sample
                    string sample = i.ToString(CultureInfo.InvariantCulture);
                    sample = sample.PadRight(10);
                    list.Add(sample);

                    for (int j = 0; j < _sampleCount; j++)
                    {
                        //add the distance matrix value to the line contents list ls
                        list.Add(_distanceMatrix[i][j].ToString("0.000000000000"));
                    }
                    //add the line contents to the sLines string list
                    //for all of the lines in the distance matrix string/file
                    sw.WriteLine((string.Join("\t", list.ToArray())));
                }
                _distanceMatrix = null;
                return sw.ToString();
            }
        }

        private static double[][] CalculateMatrix(List<List<bool>> binaryData, int samples, int markers)
        {
            var dm = new double[samples][];
            for (int i = 0; i < samples; i++)
            {
                var dmi = new double[samples];
                List<bool> bI = binaryData[i];
                for (int j = 0; j < samples; j++)
                {
                    List<bool> bJ = binaryData[j];
                    int diff = 0;
                    for (int k = 0; k < markers; k++)
                    {
                        if (bI[k] != bJ[k])
                        {
                            diff++;
                        }
                    }
                    dmi[j] = diff / (double) markers;
                }
                dm[i] = dmi;
            }
            return dm;
        }

        /// <summary>Generate a random marker set.</summary>
        private void MakeRandSet()
        {
            var hashset = new HashSet<int>();
            while (hashset.Count < _markerCount)
            {
                hashset.Add(_random.Next(0, Fingerprints.Count));
            }
            _markers = hashset.ToList();
            _markers.Sort();


            for (int i = 0; i < _sampleCount; i++)
            {
                var list = new List<bool>(_markerCount);
                for (int j = 0; j < _markerCount; j++)
                {
                    list.Add(_refBinaryData[i][_markers[j]]);
                }
                _bin.Add(list); //add the gene binary absence/presence value to the binary value list
            }
        }
    }
}