﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;

namespace CreateOptimizedCGFAssay
{
    [Serializable]
    public class MarkerSetAnalysis
    {
        /// <summary>Best markers encountered in iterations.</summary>
        private readonly MarkerSet[] _bestSymDSets;

        private readonly MarkerSet[] _bestWallaceSets;

        /// <summary>Binary BLAST report file path.</summary>
        [NonSerialized] private readonly FileInfo _binaryDataFile;

        /// <summary>Number of marker sets to create and compare against the reference marker set.</summary>
        private readonly long _iterations;

        /// <summary>Number of markers within a marker set.</summary>
        private readonly int _markerCount;

        private readonly double _markerSetClustersAt = 95d;

        /// <summary>Reference marker set comprised of entire binarized data set from user-supplied file.</summary>
        private readonly MarkerSet _referenceMarkerSet;

        /// <summary>Number of marker sets to keep. Marker sets with the lowest SymD</summary>
        private readonly int _setsToKeep;

        /// <summary>Symmetric distance counts hash.</summary>
        private readonly Dictionary<int, long> _symDCounts = new Dictionary<int, long>();

        private readonly Dictionary<double, long> _wallaceCounts = new Dictionary<double, long>();

        /// <summary>Worst marker set encountered in iterations.</summary>
        private readonly MarkerSet[] _worstSymDSets;

        private readonly MarkerSet[] _worstWallaceSets;
        private double _maxWallace = double.MinValue;
        private double _minWallace = double.MaxValue;
        private int _minSymD = int.MaxValue;
        private int _maxSymD = int.MinValue;

        private MarkerSetAnalysis(
            FileInfo binaryDataFile,
            int setsToKeep,
            long iterations,
            int markerCount,
            double clustersAt)
        {
            _binaryDataFile = binaryDataFile;
            _setsToKeep = setsToKeep;
            _bestSymDSets = new MarkerSet[_setsToKeep];
            _bestWallaceSets = new MarkerSet[_setsToKeep];
            _worstSymDSets = new MarkerSet[_setsToKeep];
            _worstWallaceSets = new MarkerSet[_setsToKeep];
            _iterations = iterations;
            _markerCount = markerCount;
            _markerSetClustersAt = clustersAt;
            //init _wallaceCounts with bins of size 0.025
            double bin = 0d;
            while (bin < 1d)
            {
                //use rounding to avoid precision error
                _wallaceCounts.Add(Math.Round(bin * 40d) / 40d, 0L);
                bin += 0.025d;
            }
        }

        public MarkerSetAnalysis(
            FileInfo binaryDataFile,
            int setsToKeep,
            long iterations,
            int markerCount,
            double clustersAt,
            double refClustersAt) 
            : this(binaryDataFile, setsToKeep, iterations, markerCount, clustersAt)
        {
            _referenceMarkerSet = new MarkerSet(_binaryDataFile, refClustersAt);
        }

        public MarkerSetAnalysis(
            FileInfo binaryDataFile,
            int setsToKeep,
            long iterations,
            int markerCount,
            double clustersAt,
            Dictionary<string, int> refClustersLow,
            Dictionary<string,int> refClustersHigh)
            : this(binaryDataFile, setsToKeep, iterations, markerCount, clustersAt)
        {
            _referenceMarkerSet = new MarkerSet(_binaryDataFile, refClustersLow, refClustersHigh);
        }

        public MarkerSetAnalysis(
            FileInfo binaryDataFile,
            int setsToKeep,
            long iterations,
            int markerCount,
            double clustersAt,
            Dictionary<string, int> refClustersLow,
            Dictionary<string,int> refClustersHigh,
            string refTree)
            : this(binaryDataFile, setsToKeep, iterations, markerCount, clustersAt)
        {
            _referenceMarkerSet = new MarkerSet(_binaryDataFile, refClustersLow, refClustersHigh, refTree);
        }

        /// <summary>Number of marker sets to create and compare against the reference marker set.</summary>
        public long Iterations { get { return _iterations; } }

        /// <summary>Number of marker sets to keep. Marker sets with the lowest SymD</summary>
        public int SetsToKeep { get { return _setsToKeep; } }

        /// <summary>Number of markers within a marker set.</summary>
        public int MarkerCount { get { return _markerCount; } }

        /// <summary>Symmetric distance counts hash.</summary>
        public Dictionary<int, long> SymDCounts { get { return _symDCounts; } }


        public Dictionary<double, long> WallaceCounts { get { return _wallaceCounts; } }


        /// <summary>Reference marker set comprised of entire binarized data set from user-supplied file.</summary>
        public MarkerSet ReferenceMarkerSet { get { return _referenceMarkerSet; } }


        /// <summary>Binary BLAST report file path.</summary>
        public FileInfo BinaryDataFile { get { return _binaryDataFile; } }

        /// <summary>Sample names.</summary>
        public List<string> Samples { get { return _referenceMarkerSet.Samples; } }

        /// <summary>Best markers encountered in iterations.</summary>
        public MarkerSet[] BestSymDSets { get { return _bestSymDSets; } }


        public MarkerSet[] BestWallaceSets { get { return _bestWallaceSets; } }

        public MarkerSet[] WorstSymDSets { get { return _worstSymDSets; } }

        public MarkerSet[] WorstWallaceSets { get { return _worstWallaceSets; } }


        public void CreateMarkerSets(BackgroundWorker bgw)
        {
            var countdown = new CountdownEvent(1);

            long count = 0;

            var random = new Random();
            const int minIterations = 2000;
            var stopwatch = new Stopwatch();
            var stopwatchLong = new Stopwatch();
            stopwatchLong.Start();

            while (count < _iterations)
            {
                stopwatch.Restart();

                countdown.Reset(1);
                //set the capacity of the list to hold the random marker sets
                var list = new List<MarkerSet>(minIterations);
                for (int i = 0; i < minIterations; i++)
                {
                    countdown.AddCount();
                    int seed = random.Next();
                    ThreadPool.QueueUserWorkItem(delegate
                                                     {
                                                         var set = new MarkerSet(_referenceMarkerSet, seed, _markerCount, _markerSetClustersAt);
                                                         list.Add(set);
                                                         MarkerSet.CalculateSymD(set);
                                                         countdown.Signal();
                                                     });
                }

                //wait for all current iterations to finish
                countdown.Signal();
                countdown.Wait();

                bool newMarkerSetAdded = false;

                int countNullBestWallace = 0;
                foreach (MarkerSet set in _bestWallaceSets)
                {
                    if (set == null)
                        countNullBestWallace++;
                }
                int countNullBestSymD = 0;
                foreach (MarkerSet set in _bestSymDSets)
                {
                    if (set == null)
                        countNullBestSymD++;
                }


                //see if any of the marker sets that have been generated are better than the ones in the best markers list
                foreach (MarkerSet set in list)
                {
                    int symD = set.SymmetricDifference;
                    if (symD > _maxSymD)
                        _maxSymD = symD;
                    if (symD < _minSymD)
                        _minSymD = symD;
                    if (_symDCounts.ContainsKey(symD))
                    {
                        _symDCounts[symD]++;
                    }
                    else
                    {
                        _symDCounts.Add(symD, 1);
                    }
                    //check if the Wallace value for the current marker set is the max or min Wallace encountered
                    double wallace = set.WallaceSetVsRefLow;
                    if (wallace > _maxWallace)
                        _maxWallace = wallace;
                    if (wallace < _minWallace)
                        _minWallace = wallace;

                    double binWallace = Math.Round(wallace * 40d) / 40d;
                    if (_wallaceCounts.ContainsKey(binWallace))
                    {
                        _wallaceCounts[binWallace]++;
                    }

                    for (int i = 0; i < _bestSymDSets.Length; i++)
                    {
                        
                        if (_bestSymDSets[i] == null)
                        {
                            _bestSymDSets[i] = set;
                            newMarkerSetAdded = true;
                            countNullBestSymD--;
                            break;
                        }
                        if(countNullBestSymD > 0)
                            continue;
                        //if the symmetric distance is lower in new marker set then replace the current best marker set with the better marker set
                        if (symD < _bestSymDSets[i].SymmetricDifference)
                        {
                            _bestSymDSets[i] = set;
                            newMarkerSetAdded = true;
                            break;
                        }
                        if (symD == _bestSymDSets[i].SymmetricDifference && 
                            wallace > _bestSymDSets[i].WallaceSetVsRefLow)
                        {
                            _bestSymDSets[i] = set;
                            newMarkerSetAdded = true;
                            break;
                        }
                    }


                    for (int i = 0; i < _bestWallaceSets.Length; i++)
                    {
                        if (_bestWallaceSets[i] == null)
                        {
                            _bestWallaceSets[i] = set;
                            newMarkerSetAdded = true;
                            countNullBestWallace--;
                            break;
                        }
                        if (countNullBestWallace > 0)
                            continue;
                        //if the Wallace value is higher in new marker set then replace the current best marker set with the better marker set
                        if (wallace > _bestWallaceSets[i].WallaceSetVsRefLow)
                        {
                            _bestWallaceSets[i] = set;
                            newMarkerSetAdded = true;
                            break;
                        }
                        //if the wallace value is the same but the SymD is lower then replace the current best marker set with the better marker set
                        if (Math.Abs(wallace - _bestWallaceSets[i].WallaceSetVsRefLow) < double.Epsilon &&
                            set.WallaceSetVsRefHigh > _bestWallaceSets[i].WallaceSetVsRefHigh)
                        {
                            _bestWallaceSets[i] = set;
                            newMarkerSetAdded = true;
                            break;
                        }
                    }

                    //see if there is a new worst marker set
                    for (int i = 0; i < _worstSymDSets.Length; i++)
                    {
                        if (_worstSymDSets[i] == null)
                        {
                            _worstSymDSets[i] = set;
                            newMarkerSetAdded = true;
                            break;
                        }
                        //if the symmetric distance is higher in new marker set then replace the current worst marker set with the current marker set
                        if (symD > _worstSymDSets[i].SymmetricDifference)
                        {
                            _worstSymDSets[i] = set;
                            newMarkerSetAdded = true;
                            break;
                        }
                    }


                    for (int i = 0; i < _worstWallaceSets.Length; i++)
                    {
                        if (_worstWallaceSets[i] == null)
                        {
                            _worstWallaceSets[i] = set;
                            newMarkerSetAdded = true;
                            break;
                        }
                        //if the Wallace value is lower in new marker set then replace the current worst marker set with the current marker set
                        if (wallace < _worstWallaceSets[i].WallaceSetVsRefLow)
                        {
                            _worstWallaceSets[i] = set;
                            newMarkerSetAdded = true;
                            break;
                        }
                    }
                }

                TimeSpan lastRun = stopwatch.Elapsed;
                TimeSpan runningTime = stopwatchLong.Elapsed;
                var timeLeft = new TimeSpan(lastRun.Ticks * (_iterations - count + minIterations) / minIterations);
                string status = string.Format("({0}/{1}); Running for {2}h{3}m{4}s. Last {5} in {6}m{7}s. Est. time left {11}d{8}h{9}m{10}s",
                                              count + minIterations,
                                              _iterations,
                                              runningTime.Hours,
                                              runningTime.Minutes,
                                              runningTime.Seconds,
                                              minIterations,
                                              lastRun.Minutes,
                                              lastRun.Seconds,
                                              timeLeft.Hours,
                                              timeLeft.Minutes,
                                              timeLeft.Seconds,
                                              timeLeft.Days);
                bgw.ReportProgress((int) (count/(double) _iterations*100d),
                                   new MarkerSetAnalysisReporter(
                                       status, 
                                       _symDCounts,
                                       _wallaceCounts,
                                       _minWallace,
                                       _maxWallace,
                                       _minSymD,
                                       _maxSymD,
                                       newMarkerSetAdded));
                count += minIterations;
            }
        }

        public void SaveToBinFile(string filename)
        {
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.None);
            formatter.Serialize(stream, this);
            stream.Close();
        }
    }
}