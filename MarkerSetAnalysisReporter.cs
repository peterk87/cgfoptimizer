using System.Collections.Generic;

namespace CreateOptimizedCGFAssay
{
    /// <summary>Class for holding progress report info for COCA analysis.</summary>
    public class MarkerSetAnalysisReporter
    {
        private readonly string _status;
        private readonly Dictionary<int, long> _symDCounts;
        private readonly Dictionary<double, long> _wallaceCounts;
        private readonly double _minWallace;
        private readonly double _maxWallace;
        private readonly int _minSymD;
        private readonly int _maxSymD;
        private readonly bool _newMarkerSetAdded;


        public MarkerSetAnalysisReporter(
            string status, 
            Dictionary<int, long> symDCounts, 
            Dictionary<double, long> wallaceCounts, 
            double minWallace, 
            double maxWallace, 
            int minSymD,
            int maxSymD,
            bool newMarkerSetAdded)
        {
            _status = status;
            _symDCounts = symDCounts;
            _wallaceCounts = wallaceCounts;
            _minWallace = minWallace;
            _maxWallace = maxWallace;
            _minSymD = minSymD;
            _maxSymD = maxSymD;
            _newMarkerSetAdded = newMarkerSetAdded;
        }

        public string Status
        {
            get { return _status; }
        }

        public Dictionary<int, long> SymDCounts
        {
            get { return _symDCounts; }
        }

        public Dictionary<double, long> WallaceCounts
        {
            get { return _wallaceCounts; }
        }

        public double MinWallace
        {
            get { return _minWallace; }
        }

        public double MaxWallace
        {
            get { return _maxWallace; }
        }

        public bool NewMarkerSetAdded
        {
            get { return _newMarkerSetAdded; }
        }

        public double MinSymD
        {
            get { return _minSymD; }
        }

        public double MaxSymD
        {
            get { return _maxSymD; }
        }
    }
}