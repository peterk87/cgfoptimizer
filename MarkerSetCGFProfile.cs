﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using BrightIdeasSoftware;

namespace CreateOptimizedCGFAssay
{
    public partial class MarkerSetCGFProfile : Form
    {
        private readonly Dictionary<int, Color> _clusterColorDict = new Dictionary<int, Color>();
        private readonly MarkerSet _markerSet;

        private readonly List<string> _orderedSamples;

        private readonly Dictionary<string, int> _strainClusterDict = new Dictionary<string, int>();

        private ToolStripMenuItem _btnCopyCGFProfileToClipboard;
        private ContextMenuStrip _cms;
        private FastObjectListView _olv;

        public MarkerSetCGFProfile(MarkerSet markerSet)
        {
            _markerSet = markerSet;
            InitializeComponent();

            InitStrainClusterDict();

            InitClusterColorsDict();

            _orderedSamples = _markerSet.GetSamplesInNJTreeOrder();

            SetupOLV();
            SetupOLVContextMenu();

            _olv.SetObjects(_orderedSamples);


            Load += (sender, args) => Text = string.Format("CGF Profile - {0} Marker Set - {1:0.0000} Wallace - {2} SymD",
                                                           _markerSet.MarkerCount,
                                                           _markerSet.WallaceSetVsRefLow,
                                                           _markerSet.SymmetricDifference);
        }

        private void InitClusterColorsDict()
        {
            var r = new Random();
            foreach (int cluster in _markerSet.ClustersLow)
            {
                if (_clusterColorDict.ContainsKey(cluster))
                    continue;
                _clusterColorDict.Add(cluster, Color.FromArgb(32, r.Next(0, 256), r.Next(0, 256), r.Next(0, 256)));
            }
        }

        private void InitStrainClusterDict()
        {
            int[] clusters = _markerSet.ClustersLow;

            for (int i = 0; i < _markerSet.Samples.Count; i++)
            {
                string sample = _markerSet.Samples[i];
                int cluster = clusters[i];
                _strainClusterDict.Add(sample, cluster);
            }
        }

        private void SetupOLV()
        {
            _olv = new FastObjectListView
                       {
                           OwnerDraw = true,
                           Dock = DockStyle.Fill,
                           Parent = this,
                           UseTranslucentSelection = true,
                           OwnerDrawnHeader = true,
                           UseCellFormatEvents = true,
                           FullRowSelect = true,
                       };

            _olv.Columns.Add(new OLVColumn
                                 {
                                     Text = "Strain",
                                     AspectGetter = o =>
                                                        {
                                                            var strain = (string) o;
                                                            return strain;
                                                        }
                                 });
            _olv.Columns.Add(new OLVColumn
                                 {
                                     Text = "Cluster",
                                     TextAlign = HorizontalAlignment.Center,
                                     HeaderTextAlign = HorizontalAlignment.Center,
                                     AspectGetter = o =>
                                                        {
                                                            var strain = (string) o;
                                                            return _strainClusterDict[strain];
                                                        }
                                 });

            _olv.Columns.Add(new OLVColumn
                                 {
                                     Text = "CGF Profile",
                                     AspectGetter = o =>
                                                        {
                                                            var strain = (string) o;
                                                            List<int> markers = _markerSet.Markers;
                                                            int index = _markerSet.Samples.IndexOf(strain);

                                                            var sb = new StringBuilder();

                                                            List<bool> bools = _markerSet.RefBinaryData[index];
                                                            foreach (int marker in markers)
                                                            {
                                                                sb.Append(bools[marker] ? "1" : "0");
                                                            }
                                                            return sb.ToString();
                                                        },
                                     RendererDelegate = delegate(EventArgs e, Graphics g, Rectangle r, object o)
                                                            {
                                                                var strain = (string) o;
                                                                List<int> markers = _markerSet.Markers;
                                                                int index = _markerSet.Samples.IndexOf(strain);

                                                                var data = new List<bool>();

                                                                List<bool> bools = _markerSet.RefBinaryData[index];
                                                                foreach (int marker in markers)
                                                                {
                                                                    data.Add(bools[marker]);
                                                                }

                                                                g.FillRectangle(new SolidBrush(Color.White), r);
                                                                g.DrawImage(GetBinaryBitmap(data.ToArray()), r);
                                                                return true;
                                                            }
                                 });
            _olv.FormatCell += (sender, args) =>
                                   {
                                       var strain = (string) args.Model;
                                       int cluster = _strainClusterDict[strain];
                                       Color color = _clusterColorDict[cluster];

                                       args.SubItem.Decorations.Clear();
                                       args.SubItem.Decorations.Add(new CellBorderDecoration
                                                                        {
                                                                            BorderPen = new Pen(color),
                                                                            FillBrush = new SolidBrush(color),
                                                                            CornerRounding = 0f,
                                                                            BoundsPadding = new Size(0, 0),
                                                                        });
                                   };
        }

        private void SetupOLVContextMenu()
        {
            _btnCopyCGFProfileToClipboard = new ToolStripMenuItem("Copy CGF Profile To Clipboard");
            _btnCopyCGFProfileToClipboard.Click += (sender, args) =>
                                                       {
                                                           var sw = new StringWriter();

                                                           sw.Write("Sample\tCluster");
                                                           for (int i = 0; i < _markerSet.MarkerCount; i++)
                                                           {
                                                               sw.Write("\t");
                                                               sw.Write(string.Format("Marker {0}", i + 1));
                                                           }

                                                           sw.WriteLine();

                                                           foreach (string sample in _markerSet.Samples)
                                                           {
                                                               sw.Write(sample);
                                                               int index = _markerSet.Samples.IndexOf(sample);
                                                               sw.Write("\t");
                                                               sw.Write(_markerSet.ClustersLow[index]);
                                                               List<int> markers = _markerSet.Markers;
                                                               List<bool> bools = _markerSet.RefBinaryData[index];
                                                               foreach (int marker in markers)
                                                               {
                                                                   sw.Write("\t");
                                                                   sw.Write(bools[marker] ? "1" : "0");
                                                               }
                                                               sw.WriteLine();
                                                           }

                                                           Clipboard.SetText(sw.ToString());
                                                       };


            _cms = new ContextMenuStrip();

            _cms.Items.Add(_btnCopyCGFProfileToClipboard);

            _olv.ContextMenuStrip = _cms;
        }

        private static Bitmap GetBinaryBitmap(bool[] data)
        {
            var bm = new UnsafeBitmap(new Bitmap(data.Length, 1));
            bm.LockBitmap();
            for (int i = 0; i < data.Length; i++)
            {
                bool next = data[i];
                for (int j = 0; j < 1; j++)
                {
                    var px = new PixelData
                                 {Blue = (next) ? (byte) 0 : byte.MaxValue, Green = (next) ? (byte) 0 : byte.MaxValue, Red = (next) ? (byte) 0 : byte.MaxValue};
                    bm.SetPixel(i, j, px);
                }
            }
            bm.UnlockBitmap();
            return bm.Bitmap;
        }
    }
}