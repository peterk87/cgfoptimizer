﻿namespace CreateOptimizedCGFAssay
{
    partial class MarkersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.olvMarkerGroups = new BrightIdeasSoftware.FastObjectListView();
            this.olvMarkers = new BrightIdeasSoftware.FastObjectListView();
            this.lblHomologFile = new System.Windows.Forms.Label();
            this.lblGeneFilesDir = new System.Windows.Forms.Label();
            this.btnClump = new System.Windows.Forms.Button();
            this.pbr = new System.Windows.Forms.ProgressBar();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblAllGenes = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.olvMarkerGroups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.olvMarkers)).BeginInit();
            this.SuspendLayout();
            // 
            // olvMarkerGroups
            // 
            this.olvMarkerGroups.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.olvMarkerGroups.CheckBoxes = false;
            this.olvMarkerGroups.Location = new System.Drawing.Point(12, 12);
            this.olvMarkerGroups.Name = "olvMarkerGroups";
            this.olvMarkerGroups.ShowGroups = false;
            this.olvMarkerGroups.Size = new System.Drawing.Size(390, 216);
            this.olvMarkerGroups.TabIndex = 0;
            this.olvMarkerGroups.UseCompatibleStateImageBehavior = false;
            this.olvMarkerGroups.View = System.Windows.Forms.View.Details;
            this.olvMarkerGroups.VirtualMode = true;
            // 
            // olvMarkers
            // 
            this.olvMarkers.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.olvMarkers.CheckBoxes = false;
            this.olvMarkers.Location = new System.Drawing.Point(420, 12);
            this.olvMarkers.Name = "olvMarkers";
            this.olvMarkers.ShowGroups = false;
            this.olvMarkers.Size = new System.Drawing.Size(197, 216);
            this.olvMarkers.TabIndex = 1;
            this.olvMarkers.UseCompatibleStateImageBehavior = false;
            this.olvMarkers.View = System.Windows.Forms.View.Details;
            this.olvMarkers.VirtualMode = true;
            // 
            // lblHomologFile
            // 
            this.lblHomologFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblHomologFile.AutoSize = true;
            this.lblHomologFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHomologFile.Location = new System.Drawing.Point(8, 240);
            this.lblHomologFile.Name = "lblHomologFile";
            this.lblHomologFile.Size = new System.Drawing.Size(106, 20);
            this.lblHomologFile.TabIndex = 2;
            this.lblHomologFile.Text = "Homolog File:";
            // 
            // lblGeneFilesDir
            // 
            this.lblGeneFilesDir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblGeneFilesDir.AutoSize = true;
            this.lblGeneFilesDir.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGeneFilesDir.Location = new System.Drawing.Point(8, 280);
            this.lblGeneFilesDir.Name = "lblGeneFilesDir";
            this.lblGeneFilesDir.Size = new System.Drawing.Size(157, 20);
            this.lblGeneFilesDir.TabIndex = 3;
            this.lblGeneFilesDir.Text = "Gene Files Directory:";
            // 
            // btnClump
            // 
            this.btnClump.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClump.Location = new System.Drawing.Point(529, 280);
            this.btnClump.Name = "btnClump";
            this.btnClump.Size = new System.Drawing.Size(88, 38);
            this.btnClump.TabIndex = 4;
            this.btnClump.Text = "Clump";
            this.btnClump.UseVisualStyleBackColor = true;
            // 
            // pbr
            // 
            this.pbr.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pbr.Location = new System.Drawing.Point(12, 303);
            this.pbr.Name = "pbr";
            this.pbr.Size = new System.Drawing.Size(100, 23);
            this.pbr.TabIndex = 5;
            this.pbr.Visible = false;
            // 
            // lblStatus
            // 
            this.lblStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(118, 308);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(59, 13);
            this.lblStatus.TabIndex = 6;
            this.lblStatus.Text = "Clumping...";
            this.lblStatus.Visible = false;
            // 
            // lblAllGenes
            // 
            this.lblAllGenes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblAllGenes.AutoSize = true;
            this.lblAllGenes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAllGenes.Location = new System.Drawing.Point(460, 240);
            this.lblAllGenes.Name = "lblAllGenes";
            this.lblAllGenes.Size = new System.Drawing.Size(157, 20);
            this.lblAllGenes.TabIndex = 7;
            this.lblAllGenes.Text = "Clump from all genes";
            // 
            // MarkersForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(629, 330);
            this.Controls.Add(this.lblAllGenes);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.pbr);
            this.Controls.Add(this.btnClump);
            this.Controls.Add(this.lblGeneFilesDir);
            this.Controls.Add(this.lblHomologFile);
            this.Controls.Add(this.olvMarkers);
            this.Controls.Add(this.olvMarkerGroups);
            this.Name = "MarkersForm";
            this.Text = "MarkersForm";
            ((System.ComponentModel.ISupportInitialize)(this.olvMarkerGroups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.olvMarkers)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private BrightIdeasSoftware.FastObjectListView olvMarkerGroups;
        private BrightIdeasSoftware.FastObjectListView olvMarkers;
        private System.Windows.Forms.Label lblHomologFile;
        private System.Windows.Forms.Label lblGeneFilesDir;
        private System.Windows.Forms.Button btnClump;
        private System.Windows.Forms.ProgressBar pbr;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label lblAllGenes;
    }
}