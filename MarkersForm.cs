﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;
using BrightIdeasSoftware;

namespace CreateOptimizedCGFAssay
{
    public partial class MarkersForm : Form
    {
        private readonly MarkerSet _markerSet;

        private readonly HashSet<string> _markers;

        private FileInfo _homologFile;

        private DirectoryInfo _geneFilesDir;

        private Dictionary<string, FileInfo> _geneFileDict;

        private List<FileInfo> _geneFiles;

        private Dictionary<string, List<string>> _markerAlleles;

        private BackgroundWorker _bgw;

        public MarkersForm(MarkerSet markerSet)
        {
            _markerSet = markerSet;
            InitializeComponent();

            var list = new List<string>();
            foreach (int i in _markerSet.Markers)
            {
                var fingerprint = _markerSet.Fingerprints[i];
                list.AddRange(_markerSet.UniqueFingerprints[fingerprint]);
            }
            _markers = new HashSet<string>(list);

            SetupMarkerGroupsOLV();

            SetupMarkersOLV();


            lblGeneFilesDir.AllowDrop = true;
            lblGeneFilesDir.DragEnter += (sender, args) => OnFileDragOverEnter(args);
            lblGeneFilesDir.DragOver += (sender, args) => OnFileDragOverEnter(args);
            lblGeneFilesDir.DragDrop += (sender, args) => OnFileDropGetGeneFiles(args);

            lblHomologFile.AllowDrop = true;
            lblHomologFile.DragEnter += (sender, args) => OnFileDragOverEnter(args);
            lblHomologFile.DragOver += (sender, args) => OnFileDragOverEnter(args);
            lblHomologFile.DragDrop += (sender, args) => OnFileDropGetHomologFile(args);

            Load += (sender, args) => Text = string.Format("{0} Marker Set - {1:0.0000} Wallace Vs Ref - {2} SymD", 
                _markerSet.MarkerCount,
                _markerSet.WallaceSetVsRefLow, 
                _markerSet.SymmetricDifference);



            _bgw = new BackgroundWorker
                       {
                           WorkerReportsProgress = true,
                       };

            _bgw.DoWork += (sender, args) => Clump(_bgw);

            _bgw.RunWorkerCompleted += (sender, args) =>
                                           {
                                               pbr.Visible = false;
                                               lblStatus.Text = string.Format("All alleles retrieved for all markers in marker set");

                                           };

            _bgw.ProgressChanged += (sender, args) =>
                                        {
                                            var t =(Tuple<int, string>) args.UserState;
                                            pbr.Value = t.Item1;
                                            lblStatus.Text = t.Item2;
                                        };
            

            btnClump.Click += (sender, args) =>
                                  {
                                      pbr.Value = 0;
                                      pbr.Maximum = _markerSet.MarkerCount;
                                      pbr.Visible = true;
                                      lblStatus.Visible = true;

                                      _bgw.RunWorkerAsync();
                                  };



            lblAllGenes.AllowDrop = true;
            lblAllGenes.DragEnter += (sender, args) => OnFileDragOverEnter(args);
            lblAllGenes.DragOver += (sender, args) => OnFileDragOverEnter(args);
            lblAllGenes.DragDrop += (sender, args) => CopyGeneFilesToMarkerDirs(args);



        }

        private void CopyGeneFilesToMarkerDirs(DragEventArgs args)
        {
            if (!args.Data.GetDataPresent(DataFormats.FileDrop)) return;

            var files = (string[]) args.Data.GetData(DataFormats.FileDrop);
            if (files.Length != 1) return;

            var geneDir = new DirectoryInfo(files[0]);
            var geneFiles = geneDir.GetFiles("*.fasta");

            var geneFileDict = new Dictionary<string, FileInfo>();

            foreach (FileInfo geneFile in geneFiles)
            {
                var geneName = geneFile.Name;
                geneName = geneName.Replace(geneFile.Extension, "");
                geneFileDict.Add(geneName, geneFile);
            }


            var dir = new FileInfo(Application.ExecutablePath).Directory;

            var markersDir = dir.CreateSubdirectory(Text);

            var dict = new Dictionary<string, int>();
            foreach (int index in _markerSet.Markers)
            {
                string fingerprint = _markerSet.Fingerprints[index];
                int fingerprintCount = _markerSet.UniqueFingerprints[fingerprint].Count;
                dict.Add(fingerprint, fingerprintCount);
            }

            int count = 1;
            //for each fingerprint/marker in the marker set
            foreach (var fingerprint in dict)
            {
                //create a directory for that fingerprint/marker
                //var markerDir = markersDir.CreateSubdirectory(string.Format("{0}_{1}_{2}", count, fingerprint.Value, fingerprint.Key));
                var markerDir = markersDir.CreateSubdirectory(string.Format("Marker_{0}_{1}", count, fingerprint.Value));
                //get the markers with that fingerprint
                List<string> markers = _markerSet.UniqueFingerprints[fingerprint.Key];
                //for each marker with the same fingerprint
                foreach (string marker in markers)
                {
                    var geneFile = geneFileDict[marker];
                    //copy gene file to marker directory
                    geneFile.CopyTo(Path.Combine(markerDir.FullName,geneFile.Name));
                }
                //increment marker count
                count++;
            }
        }

        private void Clump(BackgroundWorker bgw)
        {
            if (_markerAlleles.Count == 0 || _geneFileDict.Count == 0)
                return;

            var dir = _homologFile.Directory;

            var markersDir = dir.CreateSubdirectory(Text);

            var dict = new Dictionary<string, int>();
            foreach (int index in _markerSet.Markers)
            {
                string fingerprint = _markerSet.Fingerprints[index];
                int fingerprintCount = _markerSet.UniqueFingerprints[fingerprint].Count;
                dict.Add(fingerprint,fingerprintCount);
            }

            int count = 1;
            //for each fingerprint/marker in the marker set
            foreach (var fingerprint in dict)
            {
                //create a directory for that fingerprint/marker
                var markerDir = markersDir.CreateSubdirectory(string.Format("{0}_{1}_{2}", count, fingerprint.Value, fingerprint.Key));
                
                //get the markers with that fingerprint
                List<string> markers = _markerSet.UniqueFingerprints[fingerprint.Key];
                //for each marker with the same fingerprint
                foreach (string marker in markers)
                {
                    var alleles = _markerAlleles[marker];
                    //for each allele for the current marker
                    using (var sw = new StreamWriter(Path.Combine(markerDir.FullName, marker + ".fasta")))
                    foreach (string allele in alleles)
                    {
                        //make sure that the allele can be retrieved
                        if (allele == "-1")
                            continue;
                        //write the allele to the marker alleles file
                        WriteAlleleToMarkerFile(sw, allele);
                    }
                }

                bgw.ReportProgress(0, (new Tuple<int, string> (count, string.Format("Marker {0} - clumped {1} genes", count, markers.Count))));

                //increment marker count
                count++;
            }
        }

        private void WriteAlleleToMarkerFile(StreamWriter sw, string allele)
        {
            //get the genome name from the allele ID (<locus ID>-<gene ID>-<strain/genome>)
            string genomeName = allele.Substring(allele.LastIndexOf('-')+1);
            //get the gene file info
            FileInfo file;
            if (!_geneFileDict.TryGetValue(genomeName, out file))
                return;
            //the target header that needs to be found in the gene file
            string targetHeader = string.Format(">{0}", allele);
            //read through the gene file
            bool alleleFound = false;
            using (var sr = new StreamReader(file.FullName))
            {
                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine();
                    if (string.IsNullOrEmpty(line))
                        continue;
                    //skip non-header lines
                    if (!line.Contains(">"))
                        continue;
                    //trim any white space
                    line = line.Trim();
                    //check if the header matches the target header sequence
                    if (line != targetHeader) 
                        continue;
                    //write header to the marker allele file
                    sw.WriteLine(line);
                    //write the sequence of the allele to the marker allele file
                    while (!sr.EndOfStream)
                    {
                        line = sr.ReadLine();
                        //check that read line is not null or empty
                        if (string.IsNullOrEmpty(line))
                            continue;
                        //check that read line is not the next fasta entry
                        if (line.Contains(">"))
                            break;
                        //trim whitespace
                        line = line.Trim();
                        //write the sequence for the current fasta entry to the marker allele file
                        sw.WriteLine(line);
                    }
                    alleleFound = true;
                    break;
                }
            }
            if (!alleleFound)
                throw new Exception(string.Format("Allele {0} not found in {1}", allele, file.Name));
        }

        private void OnFileDropGetHomologFile(DragEventArgs args)
        {
            if (!args.Data.GetDataPresent(DataFormats.FileDrop))
                return;
            var filenames = (string[]) args.Data.GetData(DataFormats.FileDrop);
            if (filenames.Length != 1)
                return;


            _homologFile = new FileInfo(filenames[0]);

            using (var sr = new StreamReader(_homologFile.FullName))
            {

                var dict = new Dictionary<string, List<string>>();


                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine();
                    if (string.IsNullOrEmpty(line))
                        continue;

                    var split = line.Split('\t');

                    if (split.Length < _markerSet.SampleCount+1)
                        continue;

                    string marker = split[0];

                    if (_markers.Contains(marker))
                    {

                        if (dict.ContainsKey(marker))
                            continue;

                        var list = new List<string>();
                        for (int i = 1; i < split.Length; i++)
                        {
                            list.Add(split[i]);
                        }
                        dict.Add(marker, list);
                    }


                }
                _markerAlleles = dict;
            }

            if (_markers.Count != _markerAlleles.Count)
            {
                _markerAlleles.Clear();
                _homologFile = null;
                lblHomologFile.Text = "Homolog File:";
            }
            else
            {
                lblHomologFile.Text = string.Format("Homolog File: {0}", _homologFile.Name);
            }
        }

        private void OnFileDropGetGeneFiles(DragEventArgs args)
        {
            if (!args.Data.GetDataPresent(DataFormats.FileDrop))
                return;
            var filenames = (string[]) args.Data.GetData(DataFormats.FileDrop);
            if (filenames.Length != 1)
                return;
            var path = filenames[0];
            var fileinfo = new FileInfo(path);
            //check if directory has been dropped into label
            if (fileinfo.Extension != "")
                return;
            _geneFilesDir = new DirectoryInfo(path);
            var geneFiles = new List<FileInfo>();
            var dict = new Dictionary<string, FileInfo>();
            foreach (string sample in _markerSet.Samples)
            {
                var files = _geneFilesDir.GetFiles(sample + ".f*");
                if (files.Length == 0)
                {
                    MessageBox.Show(string.Format("{0} not found in directory {1}",
                                                  sample,
                                                  _geneFilesDir.Name));
                    return;
                }
                if (files.Length == 1)
                {
                    geneFiles.Add(files[0]);
                    dict.Add(sample, files[0]);
                }

                if (files.Length <= 1) 
                    continue;
                bool geneFileFound = false;
                foreach (FileInfo fileInfo in files)
                {
                    if (fileInfo.Extension == ".ffn")
                    {
                        geneFiles.Add(fileInfo);
                        dict.Add(sample, fileInfo);
                        geneFileFound = true;
                        break;
                    }
                }
                if (!geneFileFound)
                {
                    MessageBox.Show(string.Format("{0} not found in directory {1}",
                                                  sample,
                                                  _geneFilesDir.Name));
                    return;
                }
            }
            _geneFileDict = dict;
            _geneFiles = geneFiles;
            lblGeneFilesDir.Text = string.Format("Gene Files Directory: {0} with {1} gene files", _geneFilesDir.Name, _geneFiles.Count);
        }

        private static void OnFileDragOverEnter(DragEventArgs args)
        {
            if (args.Data.GetDataPresent(DataFormats.FileDrop))
                args.Effect = DragDropEffects.Copy;
        }

        private void SetupMarkersOLV()
        {
            olvMarkers.UseTranslucentSelection = true;
            olvMarkers.UseAlternatingBackColors = true;
            olvMarkers.FullRowSelect = true;
            olvMarkers.OwnerDraw = true;

            olvMarkers.Columns.Add(new OLVColumn
                                       {
                                           Text = "Marker",
                                           TextAlign = HorizontalAlignment.Center,
                                           FillsFreeSpace = true,
                                           AspectGetter = o =>
                                                              {
                                                                  var markerName = (string) o;
                                                                  return markerName;
                                                              }
                                       });
        }

        private void SetupMarkerGroupsOLV()
        {
            olvMarkerGroups.UseTranslucentSelection = true;
            olvMarkerGroups.UseAlternatingBackColors = true;
            olvMarkerGroups.FullRowSelect = true;
            olvMarkerGroups.OwnerDraw = true;

            olvMarkerGroups.Columns.Add(new OLVColumn
                                            {
                                                Text = "Marker Group Pattern",
                                                AspectGetter = o =>
                                                                   {
                                                                       var fingerprintIndex =
                                                                           (int) o;
                                                                       string fingerprint =
                                                                           _markerSet.Fingerprints[fingerprintIndex];
                                                                       return fingerprint;
                                                                   },
                                            });
            olvMarkerGroups.Columns.Add(new OLVColumn
                                            {
                                                Text = "Count",
                                                TextAlign = HorizontalAlignment.Center,
                                                AspectGetter = o =>
                                                                   {
                                                                       var fingerprintIndex =
                                                                           (int) o;
                                                                       string fingerprint =
                                                                           _markerSet.Fingerprints[fingerprintIndex];
                                                                       return _markerSet.UniqueFingerprints[fingerprint].Count;
                                                                   }
                                            });

            olvMarkerGroups.SetObjects(_markerSet.Markers);

            olvMarkerGroups.SelectionChanged += (sender, args) =>
                                                    {
                                                        var list = new List<string>();
                                                        foreach (var o in olvMarkerGroups.SelectedObjects)
                                                        {
                                                            var fingerprintIndex =
                                                                (int) o;
                                                            string fingerprint =
                                                                _markerSet.Fingerprints[fingerprintIndex];
                                                            list.AddRange(_markerSet.UniqueFingerprints[fingerprint]);
                                                        }
                                                        olvMarkers.SetObjects(list);
                                                        olvMarkers.RefreshObjects(list);
                                                    };
        }
    }
}
