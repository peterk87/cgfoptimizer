﻿namespace CreateOptimizedCGFAssay
{
    public static class NJ
    {

        public static NJTree NeighborJoining(DistanceMatrix matrix)
        {
            //init R and R2 vectors
            matrix.InitR();

            //alloc and init vertex vector for tree construction
            var vertex = new NJVertex(matrix);

            //iterate until the working distance matrix has only 2 entries
            while (vertex.ActiveNodes > 2)
            {
                int a, b;
                //Find the global minimum transformed distance from the distance matrix
                matrix.FindMinTransformedDistance(out a, out b);

                //Build the tree by removing nodes a and b from the vertex array
                //and inserting a new internal node which joins a and b.  Collapse
                //the vertex array similarly to how the distance matrix and r and r2 
                //are compacted. 
                vertex.Decompose(a, b, false);

                matrix.ComputeR(a,b);

                matrix.Collapse(a,b);
            }

            NJTree tree = vertex.Decompose(0, 1, true);

            return tree;
        }

    }
}
