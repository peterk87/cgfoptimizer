﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;

namespace CreateOptimizedCGFAssay
{
    [Serializable]
    public class NJTree
    {
        private  NJTree _left;
        private  NJTree _right;
        private  int _taxaIndex = -1;

        public NJTree(){}

        public NJTree(int index)
        {
            _taxaIndex = index;
        }

        public NJTree(NJTree left, NJTree right)
        {
            _left = left;
            _right = right;
        }

        public NJTree Left
        {
            get { return _left; }
        }

        public NJTree Right
        {
            get { return _right; }
        }

        public double BranchLength { get; set; }

        public int TaxaIndex
        {
            get { return _taxaIndex; }
        }

        public bool IsLeaf
        {
            get { return _taxaIndex != -1; }
        }

        public void SetRightTree(NJTree right)
        {
            _right = right;
        }

        public void SetLeftTree(NJTree left)
        {
            _left = left;
        }

        public string Write()
        {
            var sb = new StringBuilder();

            Write(this, this, ref sb);
            sb.Append(";");
            return sb.ToString();
        }

        private void Write(NJTree tree, NJTree root, ref StringBuilder sb)
        {
            if (tree == null) return;
            if (tree.IsLeaf)
            {
                sb.Append(string.Format("{0}:{1}", tree.TaxaIndex,
                                        tree.BranchLength < 0 ? 0.0 : tree.BranchLength));
            }
            else
            {
                if (tree.Left != null && tree.Right != null)
                {
                    sb.Append("(");
                }
                if (tree.Left != null)
                {
                    Write(tree.Left, root, ref sb);
                }
                if (tree.Left != null && tree.Right != null)
                {
                    sb.Append(",");
                }
                if (tree.Right != null)
                {
                    Write(tree.Right, root, ref sb);
                }

                if (tree != root.Left)
                {
                    if (tree.Left != null && tree.Right != null)
                    {
                        sb.Append(tree != root
                                      ? string.Format("):{0}", tree.BranchLength < 0 ? 0.0 : tree.BranchLength)
                                      : ")");
                    }
                }
                else
                {
                    sb.Append(")");
                }
            }
        }

        public List<int> GetTaxaIndexList()
        {
            var list = new List<int>();
            GetTaxaIndexList(this, ref list);
            return list;
        }

        private static void GetTaxaIndexList(NJTree tree, ref List<int> indices)
        {
            if (tree == null) return;
            if (tree.IsLeaf)
            {
                indices.Add(tree.TaxaIndex);
            }
            else
            {
                if (tree.Left != null)
                {
                    GetTaxaIndexList(tree.Left, ref indices);
                }
                if (tree.Right != null)
                {
                    GetTaxaIndexList(tree.Right, ref indices);
                }
            }
        }
    }
}