﻿using System.Collections.Generic;

namespace CreateOptimizedCGFAssay
{
    public class NJVertex
    {
        private int _activeNodes;
        public int ActiveNodes { get { return _activeNodes; } }
        public int Size { get { return _size; } }
        public List<NJTree> Nodes { get { return _nodes; } }
        public DistanceMatrix Matrix { get { return _matrix; } }
        private int _size;

        private List<NJTree> _nodes;

        private DistanceMatrix _matrix;

        /// <summary>
        /// Construct a vertex, which we will use to construct our tree 
        /// in a true bottom-up approach.  The vertex construct is 
        /// basically the center node in the initial star topology.
        /// </summary>
        /// <param name="distanceMatrix"></param>
        public NJVertex(DistanceMatrix distanceMatrix)
        {
            _matrix = distanceMatrix;
            //set size of nodes array
            _nodes = new List<NJTree>(_matrix.NumTaxa);
            _activeNodes = _matrix.NumTaxa;
            _size = _matrix.NumTaxa;

            //init nodes
            for (int i = 0; i < _size; i++)
            {
                _nodes.Add(new NJTree(i));
            }
        }

        /// <summary>This function decomposes the star by creating new internal nodes
        /// and joining two existing tree nodes to it</summary>
        /// <param name="x">1st group index to join</param>
        /// <param name="y">2nd group index to join</param>
        /// <param name="lastFlag">Last two nodes?</param>
        /// <returns>A neighbor joining tree</returns>
        public NJTree Decompose(int x, int y, bool lastFlag)
        {

            double x2Clade, y2Clade;
            int dI = _matrix.DIndex;
            int r2I = _matrix.R2Index;
            
            if (lastFlag)
            {
                x2Clade = _matrix.Distances[DistanceMatrix.NJMap(x, y, _matrix.Size) + dI];
            }
            else
            {
                x2Clade = (_matrix.Distances[DistanceMatrix.NJMap(x, y, _matrix.Size)+dI] / 2d) +
                    ((_matrix.R2[x +r2I] - _matrix.R2[y +r2I]) / 2d);
            }
            _nodes[x].BranchLength = x2Clade;


            if (lastFlag)
            {
                y2Clade = _matrix.Distances[DistanceMatrix.NJMap(x, y, _matrix.Size) + dI];
            }
            else
            {
                y2Clade = (_matrix.Distances[DistanceMatrix.NJMap(x, y, _matrix.Size) + dI] / 2d) +
                    ((_matrix.R2[y+r2I] - _matrix.R2[x+r2I]) / 2d);
            }
            _nodes[y].BranchLength = y2Clade;

            var newNode = new NJTree(_nodes[x], _nodes[y]);

            if (lastFlag)
                return newNode;

            _nodes[x] = newNode;
            _nodes[y] = _nodes[0];

            _nodes.RemoveAt(0);
            _activeNodes--;

            return newNode;
        }

    }
}
