﻿using System.Globalization;
using System.IO;
using System.Text;

namespace CreateOptimizedCGFAssay
{
    /// <summary></summary>
    public static class NewickParser
    {
        public static NJTree Parse(string tree)
        {
            return Parse(new StringReader(tree));
        }

        private static NJTree Parse(TextReader textReader)
        {
            NJTree tree = GetNode(textReader, true);

            if (textReader.Peek() != ';')
            {
                if (textReader.Peek() == ':')
                {
                    ReadChar(textReader);
                }

            }

            textReader.Close();
            return tree;
        }

        /// <summary>Get collection of (Branche and Leaf) nodes</summary>
        /// <param name="textReader"> </param>
        /// <param name="isRoot"></param>
        /// <returns>PhylogeneticNode object</returns>
        private static NJTree GetNode(TextReader textReader, bool isRoot)
        {
            NJTree node;
            double edge;
            char firstPeek = Peek(textReader);
            if (firstPeek == '(')
            {
                node = GetBranch(textReader, isRoot);
            }
            else
            {
                node = GetLeaf(textReader);
            }

            if (isRoot)
            {
                edge = double.NaN;

                char secondPeek = Peek(textReader);
                if (secondPeek == ':')
                {   // move to next char after ":"
                    ReadChar(textReader);
                }
            }
            else
            {
                char thirdPeek = Peek(textReader);
                if (thirdPeek == ':')
                {
                    //move to next char after ":"
                    char colon = ReadChar(textReader);
                }
                edge = ReadLength(textReader);
            }
            node.BranchLength = edge;
            return node;
        }

        /// <summary>Gets the Branch node</summary>
        /// <param name="textReader"> </param>
        /// <param name="isRoot"></param>
        /// <returns>Branch object</returns>
        private static NJTree GetBranch(TextReader textReader, bool isRoot)
        {
            var branch = new NJTree();

            bool firstBranch = true;
            while (true)
            {
                char peek = Peek(textReader);
                if (!firstBranch && peek == ')')
                {
                    break;
                }

                char c = ReadChar(textReader);
                if (firstBranch)
                {
                    firstBranch = false;
                }
                var tmp = GetNode(textReader, false);

                if (branch.Left == null)
                {
                    branch.SetLeftTree(tmp);
                }
                else if (branch.Right == null)
                {
                    branch.SetRightTree(tmp);
                }
                else
                {
                    var newNode = new NJTree(branch.Left, branch.Right);
                    branch.SetLeftTree(newNode);
                    branch.SetRightTree(tmp);
                }

            }
            //move to next char of ")"
            char nextCloseChar = ReadChar(textReader);

            return branch;
        }

        /// <summary>Get the Leaf node</summary>
        /// <returns>Leaf object</returns>
        private static NJTree GetLeaf(TextReader textReader)
        {
            var sb = new StringBuilder();

            while (true)
            {
                char peek = Peek(textReader);
                if (peek == ':')
                {
                    break;
                }
                sb.Append(ReadChar(textReader));
            }

            var leaf = new NJTree(int.Parse(sb.ToString()));
            return leaf;
        }

        /// <summary>Peeks the TextReader char by char</summary>
        /// <returns>a character</returns>
        private static char Peek(TextReader textReader)
        {
            while (true)
            {
                int peek = textReader.Peek();
                char c = (char)peek;
                //Environment.NewLine
                if (c != '\r' && c != '\n')
                {
                    return c;
                }
                textReader.Read();
            }
        }

        /// <summary>Reads Length</summary>
        /// <returns>length</returns>
        private static double ReadLength(TextReader textReader)
        {
            var sb = new StringBuilder();
            while (true)
            {
                char peek = Peek(textReader);
                if (!"-0123456789.E ".Contains(peek.ToString(CultureInfo.InvariantCulture)))
                {
                    break;
                }
                sb.Append(ReadChar(textReader));
            }
            // consider -ive and +ive length
            return double.Parse(sb.ToString(), CultureInfo.InvariantCulture);
        }

        /// <summary>Reads current character</summary>
        /// <returns>a character</returns>
        private static char ReadChar(TextReader textReader)
        {
            while (true)
            {
                var c = (char)textReader.Read();
                if (c != '\r' && c != '\n')
                {
                    return c;
                }
            }
        }

    }
}
