namespace CreateOptimizedCGFAssay
{
    public struct PixelData
    {
        public byte Blue;
        public byte Green;
        public byte Red;
    }
}