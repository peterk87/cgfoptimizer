﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using BrightIdeasSoftware;

namespace CreateOptimizedCGFAssay
{
    public partial class ResultsForm : Form
    {
        private readonly MarkerSetAnalysis _analysis;

        private readonly List<SymDCount> _symDCounts = new List<SymDCount>();
        private readonly List<WallaceCount> _wallaceCounts = new List<WallaceCount>();

        public ResultsForm(MarkerSetAnalysis analysis)
        {
            InitializeComponent();

            _analysis = analysis;

            SetupResultsOLV();


            var listSets = new List<MarkerSet>();

            listSets.Add(_analysis.ReferenceMarkerSet);
            listSets.AddRange(_analysis.BestSymDSets);
            listSets.AddRange(_analysis.WorstSymDSets);
            listSets.AddRange(_analysis.BestWallaceSets);
            listSets.AddRange(_analysis.WorstWallaceSets);

            olvResults.SetObjects(listSets);

            SetupSymDChart();
            SetupWallaceChart();

            chartSymD.MouseClick += (sender, args) => CopySymDChartData(args);
            chartWallace.MouseClick += (sender, args) => CopyWallaceChartData(args);
            btnSaveBinaryFile.Click += (sender, args) => SaveAnalysis();

            Load +=
                (sender, args) =>
                Text =
                string.Format("Marker Set Analysis - {0} Markers - {1} Iterations",
                              _analysis.MarkerCount,
                              _analysis.Iterations);
            btnShowCGFProfile.Click += (sender, args) =>
                                           {
                                               foreach (object o in olvResults.SelectedObjects)
                                               {
                                                   var markerSet = (MarkerSet) o;
                                                   new MarkerSetCGFProfile(markerSet).Show();
                                               }
                                           };
        }

        private void SetupResultsOLV()
        {
            olvResults.UseAlternatingBackColors = true;
            olvResults.OwnerDraw = true;
            olvResults.FullRowSelect = true;
            olvResults.GridLines = true;
            olvResults.UseTranslucentSelection = true;
            olvResults.ContextMenuStrip = cmsResults;

            olvResults.Columns.Add(new OLVColumn("Markers", "MarkerCount"));
            olvResults.Columns.Add(new OLVColumn("SymD", "SymmetricDifference"));
            olvResults.Columns.Add(new OLVColumn("Wallace - Set vs Ref - Low", "WallaceSetVsRefLow")
            {
                AspectToStringFormat = "{0:0.0000}"
            });
            olvResults.Columns.Add(new OLVColumn("Wallace - Ref vs Set - Low", "WallaceRefVsSetLow")
            {
                AspectToStringFormat = "{0:0.0000}"
            });
            olvResults.Columns.Add(new OLVColumn("Rand - Low", "RandLow")
            {
                AspectToStringFormat = "{0:0.0000}"
            });
            olvResults.Columns.Add(new OLVColumn("Adjusted Rand - Low", "AdjustedRandLow")
            {
                AspectToStringFormat = "{0:0.0000}"
            });
            olvResults.Columns.Add(new OLVColumn("Wallace - Set vs Ref - High", "WallaceSetVsRefHigh")
            {
                AspectToStringFormat = "{0:0.0000}"
            });
            olvResults.Columns.Add(new OLVColumn("Wallace - Ref vs Set - High", "WallaceRefVsSetHigh")
            {
                AspectToStringFormat = "{0:0.0000}"
            });
            olvResults.Columns.Add(new OLVColumn("Rand - High", "RandHigh")
            {
                AspectToStringFormat = "{0:0.0000}"
            });
            olvResults.Columns.Add(new OLVColumn("Adjusted Rand - High", "AdjustedRandHigh")
            {
                AspectToStringFormat = "{0:0.0000}"
            });
            olvResults.Columns.Add(new OLVColumn("SID", "SimpsonsIndexOfDiversity")
            {
                AspectToStringFormat = "{0:0.0000}"
            });

            btnCopyNewickTreeToClipboard.Click += (sender, args) => CopyNewickTreeToClipboard();
            btnCopyDistanceMatrixToClipboard.Click += (sender, args) => CopyDistanceMatrixToClipboard();
            btnCopyClusterNumbersToClipboard.Click += (sender, args) => CopyClusterNumbersToClipboard();
            btnShowMarkers.Click += (sender, args) => ShowMarkers();
        }

        private void CopyClusterNumbersToClipboard()
        {
            var sb = new StringBuilder();
            var list = new List<string> {"Samples", "Reference_Low", "Reference_High"};

            List<string> sampleNames = _analysis.ReferenceMarkerSet.Samples;
            var markerSets = new List<MarkerSet>();

            foreach (object o in olvResults.SelectedObjects)
            {
                var markerSet = (MarkerSet) o;
                markerSets.Add(markerSet);
                list.Add(string.Format("{0}Markers_{1}SymD_{2:0.0000}Wallace",
                                       markerSet.MarkerCount,
                                       markerSet.SymmetricDifference,
                                       markerSet.WallaceSetVsRefLow));
            }
            sb.AppendLine(string.Join("\t", list));

            for (int i = 0; i < sampleNames.Count; i++)
            {
                list.Clear();
                list.Add(sampleNames[i]);
                list.Add(_analysis.ReferenceMarkerSet.ClustersLow[i].ToString(CultureInfo.InvariantCulture));
                list.Add(_analysis.ReferenceMarkerSet.ClustersHigh[i].ToString(CultureInfo.InvariantCulture));
                foreach (MarkerSet markerSet in markerSets)
                {
                    list.Add(markerSet.ClustersLow[i].ToString(CultureInfo.InvariantCulture));
                }
                sb.AppendLine(string.Join("\t", list));
            }
            Clipboard.SetText(sb.ToString());
        }

        private void CopyDistanceMatrixToClipboard()
        {
            var sb = new StringBuilder();
            foreach (object o in olvResults.SelectedObjects)
            {
                var markerSet = (MarkerSet) o;
                sb.AppendLine(markerSet.DistMatrix);
            }
            Clipboard.SetText(sb.ToString());
        }

        private void CopyNewickTreeToClipboard()
        {
            var sb = new StringBuilder();
            foreach (object o in olvResults.SelectedObjects)
            {
                var markerSet = (MarkerSet) o;
                sb.AppendLine(markerSet.Tree);
            }

            Clipboard.SetText(sb.ToString());
        }

        private void ShowMarkers()
        {
            foreach (object o in olvResults.SelectedObjects)
            {
                var markerSet = (MarkerSet) o;
                new MarkersForm(markerSet).Show();
            }
        }

        private void SaveAnalysis()
        {
            using (var sfd = new SaveFileDialog())
            {
                sfd.AddExtension = true;
                sfd.FileName =
                    string.Format(string.Format("CGF_Marker_Analysis-{0}_markers-{1}_iterations-{2}_.bin",
                                                _analysis.MarkerCount,
                                                _analysis.Iterations,
                                                DateTime.Now.ToString("MM_dd_yyyy")));
                sfd.DefaultExt = "bin";
                if (sfd.ShowDialog() != DialogResult.OK) return;

                _analysis.SaveToBinFile(sfd.FileName);
            }
        }

        private void CopySymDChartData(MouseEventArgs args)
        {
            if (args.Button == MouseButtons.Right)
            {
                var sw = new StringWriter();

                sw.WriteLine("SymD\tCounts");
                foreach (SymDCount symDCount in _symDCounts)
                {
                    sw.WriteLine(string.Format("{0}\t{1}", symDCount.SymD, symDCount.Count));
                }

                Clipboard.SetText(sw.ToString());
            }
        }

        private void CopyWallaceChartData(MouseEventArgs args)
        {
            if (args.Button == MouseButtons.Right)
            {
                var sw = new StringWriter();

                sw.WriteLine("Wallace\tCounts");
                foreach (WallaceCount wallaceCount in _wallaceCounts)
                {
                    sw.WriteLine(string.Format("{0}\t{1}", wallaceCount.Wallace, wallaceCount.Count));
                }

                Clipboard.SetText(sw.ToString());
            }
        }

        private void SetupSymDChart()
        {
            Dictionary<int, long> dict = _analysis.SymDCounts;

            var keylist = new List<int>();
            foreach (int key in dict.Keys)
            {
                keylist.Add(key);
            }

            keylist.Sort();
            foreach (int i in keylist)
            {
                long count = dict[i];
                _symDCounts.Add(new SymDCount(i, count));
            }
            chartSymD.Legends.Clear();

            chartSymD.DataSource = _symDCounts;
            chartSymD.Series[0].XValueMember = "SymD";
            Axis xaxis = chartSymD.ChartAreas[0].AxisX;
            xaxis.Interval = 10;
            xaxis.MajorGrid.Enabled = false;
            xaxis.MinorTickMark.Enabled = true;
            xaxis.MinorTickMark.Interval = 2;
            xaxis.Title = "SymD";
            Axis yaxis = chartSymD.ChartAreas[0].AxisY;
            yaxis.Title = "Count";
            yaxis.MajorGrid.Enabled = false;
            chartSymD.Series[0].YValueMembers = "Count";
            chartSymD.DataBind();
        }

        private void SetupWallaceChart()
        {
            Dictionary<double, long> dict = _analysis.WallaceCounts;

            var keylist = new List<double>();
            foreach (double key in dict.Keys)
            {
                keylist.Add(key);
            }

            keylist.Sort();
            foreach (double i in keylist)
            {
                long count = dict[i];
                _wallaceCounts.Add(new WallaceCount(i, count));
            }
            chartWallace.Legends.Clear();

            chartWallace.DataSource = _wallaceCounts;
            chartWallace.Series[0].XValueMember = "Wallace";
            Axis xaxis = chartWallace.ChartAreas[0].AxisX;
            xaxis.Interval = 0.1;
            xaxis.MajorGrid.Enabled = false;
            xaxis.MinorTickMark.Enabled = true;
            xaxis.MinorTickMark.Interval = 2;
            xaxis.Title = "Wallace";
            Axis yaxis = chartWallace.ChartAreas[0].AxisY;
            yaxis.Title = "Count";
            yaxis.MajorGrid.Enabled = false;
            chartWallace.Series[0].YValueMembers = "Count";
            chartWallace.DataBind();
        }
    }
}