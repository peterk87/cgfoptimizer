﻿namespace CreateOptimizedCGFAssay
{
    partial class ResultsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ResultsForm));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.chartSymD = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chartWallace = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.olvResults = new BrightIdeasSoftware.FastObjectListView();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.ddbFile = new System.Windows.Forms.ToolStripDropDownButton();
            this.btnSaveBinaryFile = new System.Windows.Forms.ToolStripMenuItem();
            this.ddbView = new System.Windows.Forms.ToolStripDropDownButton();
            this.btnClusters = new System.Windows.Forms.ToolStripMenuItem();
            this.cmsResults = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.btnCopyNewickTreeToClipboard = new System.Windows.Forms.ToolStripMenuItem();
            this.btnCopyDistanceMatrixToClipboard = new System.Windows.Forms.ToolStripMenuItem();
            this.btnCopyClusterNumbersToClipboard = new System.Windows.Forms.ToolStripMenuItem();
            this.btnShowMarkers = new System.Windows.Forms.ToolStripMenuItem();
            this.btnShowCGFProfile = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartSymD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartWallace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.olvResults)).BeginInit();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.cmsResults.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.olvResults);
            this.splitContainer1.Size = new System.Drawing.Size(509, 414);
            this.splitContainer1.SplitterDistance = 192;
            this.splitContainer1.TabIndex = 0;
            // 
            // splitContainer2
            // 
            this.splitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.chartSymD);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.chartWallace);
            this.splitContainer2.Size = new System.Drawing.Size(509, 192);
            this.splitContainer2.SplitterDistance = 255;
            this.splitContainer2.TabIndex = 0;
            // 
            // chartSymD
            // 
            chartArea1.Name = "ChartArea1";
            this.chartSymD.ChartAreas.Add(chartArea1);
            this.chartSymD.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Name = "Legend1";
            this.chartSymD.Legends.Add(legend1);
            this.chartSymD.Location = new System.Drawing.Point(0, 0);
            this.chartSymD.Name = "chartSymD";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chartSymD.Series.Add(series1);
            this.chartSymD.Size = new System.Drawing.Size(251, 188);
            this.chartSymD.TabIndex = 0;
            this.chartSymD.Text = "chart1";
            // 
            // chartWallace
            // 
            chartArea2.Name = "ChartArea1";
            this.chartWallace.ChartAreas.Add(chartArea2);
            this.chartWallace.Dock = System.Windows.Forms.DockStyle.Fill;
            legend2.Name = "Legend1";
            this.chartWallace.Legends.Add(legend2);
            this.chartWallace.Location = new System.Drawing.Point(0, 0);
            this.chartWallace.Name = "chartWallace";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.chartWallace.Series.Add(series2);
            this.chartWallace.Size = new System.Drawing.Size(246, 188);
            this.chartWallace.TabIndex = 1;
            this.chartWallace.Text = "chart1";
            // 
            // olvResults
            // 
            this.olvResults.CheckBoxes = false;
            this.olvResults.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olvResults.Location = new System.Drawing.Point(0, 0);
            this.olvResults.Name = "olvResults";
            this.olvResults.ShowGroups = false;
            this.olvResults.Size = new System.Drawing.Size(505, 214);
            this.olvResults.TabIndex = 0;
            this.olvResults.UseCompatibleStateImageBehavior = false;
            this.olvResults.View = System.Windows.Forms.View.Details;
            this.olvResults.VirtualMode = true;
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.splitContainer1);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(509, 414);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(509, 439);
            this.toolStripContainer1.TabIndex = 1;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip1);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ddbFile,
            this.ddbView});
            this.toolStrip1.Location = new System.Drawing.Point(3, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(95, 25);
            this.toolStrip1.TabIndex = 0;
            // 
            // ddbFile
            // 
            this.ddbFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ddbFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnSaveBinaryFile});
            this.ddbFile.Image = ((System.Drawing.Image)(resources.GetObject("ddbFile.Image")));
            this.ddbFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ddbFile.Name = "ddbFile";
            this.ddbFile.Size = new System.Drawing.Size(38, 22);
            this.ddbFile.Text = "File";
            // 
            // btnSaveBinaryFile
            // 
            this.btnSaveBinaryFile.Name = "btnSaveBinaryFile";
            this.btnSaveBinaryFile.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.btnSaveBinaryFile.Size = new System.Drawing.Size(212, 22);
            this.btnSaveBinaryFile.Text = "Save To Binary File";
            // 
            // ddbView
            // 
            this.ddbView.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ddbView.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnClusters});
            this.ddbView.Image = ((System.Drawing.Image)(resources.GetObject("ddbView.Image")));
            this.ddbView.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ddbView.Name = "ddbView";
            this.ddbView.Size = new System.Drawing.Size(45, 22);
            this.ddbView.Text = "View";
            // 
            // btnClusters
            // 
            this.btnClusters.Name = "btnClusters";
            this.btnClusters.Size = new System.Drawing.Size(252, 22);
            this.btnClusters.Text = "Clusters For Comparing Partitions";
            // 
            // cmsResults
            // 
            this.cmsResults.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnCopyNewickTreeToClipboard,
            this.btnCopyDistanceMatrixToClipboard,
            this.btnCopyClusterNumbersToClipboard,
            this.btnShowMarkers,
            this.btnShowCGFProfile});
            this.cmsResults.Name = "cmsResults";
            this.cmsResults.Size = new System.Drawing.Size(264, 114);
            // 
            // btnCopyNewickTreeToClipboard
            // 
            this.btnCopyNewickTreeToClipboard.Name = "btnCopyNewickTreeToClipboard";
            this.btnCopyNewickTreeToClipboard.Size = new System.Drawing.Size(263, 22);
            this.btnCopyNewickTreeToClipboard.Text = "Copy Newick Tree to Clipboard";
            // 
            // btnCopyDistanceMatrixToClipboard
            // 
            this.btnCopyDistanceMatrixToClipboard.Name = "btnCopyDistanceMatrixToClipboard";
            this.btnCopyDistanceMatrixToClipboard.Size = new System.Drawing.Size(263, 22);
            this.btnCopyDistanceMatrixToClipboard.Text = "Copy Distance Matrix to Clipboard";
            // 
            // btnCopyClusterNumbersToClipboard
            // 
            this.btnCopyClusterNumbersToClipboard.Name = "btnCopyClusterNumbersToClipboard";
            this.btnCopyClusterNumbersToClipboard.Size = new System.Drawing.Size(263, 22);
            this.btnCopyClusterNumbersToClipboard.Text = "Copy Cluster Numbers to Clipboard";
            // 
            // btnShowMarkers
            // 
            this.btnShowMarkers.Name = "btnShowMarkers";
            this.btnShowMarkers.Size = new System.Drawing.Size(263, 22);
            this.btnShowMarkers.Text = "Show Markers";
            // 
            // btnShowCGFProfile
            // 
            this.btnShowCGFProfile.Name = "btnShowCGFProfile";
            this.btnShowCGFProfile.Size = new System.Drawing.Size(263, 22);
            this.btnShowCGFProfile.Text = "Show CGF Profile";
            // 
            // ResultsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(509, 439);
            this.Controls.Add(this.toolStripContainer1);
            this.Name = "ResultsForm";
            this.Text = "ResultsForm";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartSymD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartWallace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.olvResults)).EndInit();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.cmsResults.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartSymD;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripDropDownButton ddbFile;
        private System.Windows.Forms.ToolStripMenuItem btnSaveBinaryFile;
        private System.Windows.Forms.ToolStripDropDownButton ddbView;
        private System.Windows.Forms.ToolStripMenuItem btnClusters;
        private BrightIdeasSoftware.FastObjectListView olvResults;
        private System.Windows.Forms.ContextMenuStrip cmsResults;
        private System.Windows.Forms.ToolStripMenuItem btnCopyNewickTreeToClipboard;
        private System.Windows.Forms.ToolStripMenuItem btnCopyDistanceMatrixToClipboard;
        private System.Windows.Forms.ToolStripMenuItem btnCopyClusterNumbersToClipboard;
        private System.Windows.Forms.ToolStripMenuItem btnShowMarkers;
        private System.Windows.Forms.ToolStripMenuItem btnShowCGFProfile;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartWallace;
    }
}