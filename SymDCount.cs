namespace CreateOptimizedCGFAssay
{
    public class SymDCount
    {
        public int SymD { get; set; }
        public long Count { get; set; }

        public SymDCount(int symD, long count)
        {
            SymD = symD;
            Count = count;
        }
    }
}