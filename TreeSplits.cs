﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreateOptimizedCGFAssay
{
    [Serializable]
    public class TreeSplits
    {
        private readonly NJTree _root;

        private readonly List<NJTree> _splits = new List<NJTree>();
        public int SplitCount { get { return _splits.Count; } }

        private readonly List<HashSet<int>> _splitGroups = new List<HashSet<int>>();

        private readonly List<string> _splitIDs = new List<string>();
        public List<string> SplitIDs { get { return _splitIDs; } }

        private readonly int _numTaxa;

        public TreeSplits(NJTree root, int numTaxa)
        {
            _root = root;
            _numTaxa = numTaxa;

            if (!_root.Left.IsLeaf)
            {
                if(!_root.Right.IsLeaf)
                {
                    _splits.Add(_root.Left);
                    GetSplits(_root.Left, ref _splits);
                    //_splits.Add(_root.Right);
                    GetSplits(_root.Right, ref _splits);
                }
                else
                {
                    //_splits.Add(_root.Left);
                    GetSplits(_root.Left, ref _splits);
                }
            }
            else
            {
                //_splits.Add(_root.Right);
                GetSplits(_root.Right, ref _splits);
            }


            //GetSplits(_root, ref _splits);
            foreach (NJTree tree in _splits)
            {
                var hash = new HashSet<int>();
                GetTaxa(tree, ref hash);
                _splitGroups.Add(hash);
            }
            GetSplitIDs();
        }

        private static void GetSplits(NJTree tree, ref List<NJTree> splits)
        {
            if (tree.IsLeaf) return;
            if (!tree.Left.IsLeaf)
            {
                splits.Add(tree.Left);
                GetSplits(tree.Left, ref splits);
            }
            if (!tree.Right.IsLeaf)
            {
                splits.Add(tree.Right);
                GetSplits(tree.Right, ref splits);
            }
        }

        private static void GetTaxa(NJTree tree, ref HashSet<int> taxa)
        {
            if (tree.IsLeaf)
            {
                taxa.Add(tree.TaxaIndex);
            }
            else
            {
                if (tree.Left != null)
                {
                    GetTaxa(tree.Left, ref taxa);
                }
                if (tree.Right != null)
                {
                    GetTaxa(tree.Right, ref taxa);
                }
            }
        }

        private void GetSplitIDs()
        {
            var sb = new StringBuilder();
            foreach (HashSet<int> set in _splitGroups)
            {
                sb.Clear();
                bool setContains0 = set.Contains(0);

                for (int i = 0; i < _numTaxa; i++)
                {
                    if (set.Contains(i))
                    {
                        sb.Append(setContains0 ? "0" : "1");
                    }
                    else
                    {
                        sb.Append(setContains0 ? "1" : "0");
                    }
                }

                string splitID = sb.ToString();
                _splitIDs.Add(splitID);
            }
        }

        public static int GetDifferences(List<string> splitIDs1, List<string> splitIDs2)
        {
            var dict = new Dictionary<string, int>();

            foreach (string s in splitIDs1)
            {
                dict.Add(s, 1);
            }

            foreach (string s in splitIDs2)
            {
                if (dict.ContainsKey(s))
                {
                    dict[s]++;
                }
                else
                {
                    dict.Add(s, 1);
                }
            }

            int count = 0;
            foreach (string s in dict.Keys)
            {
                if (dict[s] == 1) count++;
            }
            return count;
        }

    }



}
