namespace CreateOptimizedCGFAssay
{
    public class WallaceCount
    {
        private readonly double _wallace;
        private readonly long _count;

        public WallaceCount(double wallace, long count)
        {
            _wallace = wallace;
            _count = count;
        }

        public double Wallace { get { return _wallace; } }
        public long Count { get { return _count; } }
    }
}